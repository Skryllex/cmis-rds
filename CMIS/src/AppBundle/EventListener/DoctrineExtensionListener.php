<?php
/**
 * @file
 * Listener for Bundle DoctrineExtensions.
 */
namespace AppBundle\EventListener;

use AppBundle\Enum\Service;
use SylrSyksSoftSymfony\CoreBundle\Enum\Gedmo;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 *
 * @link http://www.sergiolepore.net/2012/12/04/doctrine2-comportamiento-softdelete-en-symfony2/
 */
class DoctrineExtensionListener implements ContainerAwareInterface
{

    /**
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Non-PHPdoc
     *
     * @see \Symfony\Component\DependencyInjection\ContainerAwareInterface::setContainer()
     */
    public function setContainer(ContainerInterface $container = NULL)
    {
        $this->container = $container;
    }

    /**
     * Set locale.
     *
     * @param GetResponseEvent $event
     *            Application event.
     */
    public function onLateKernelRequest(GetResponseEvent $event)
    {
        $translatable = $this->container->get(Gedmo::ListenerTranslatable);
        $translatable->setTranslatableLocale($event->getRequest()->getLocale());
    }

    /**
     * Initialize listeners.
     *
     * @param GetResponseEvent $event
     *            Application event.
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // Logable listener.
        $securityContext = $this->container->get(Service::SecurityContextIdentifier, ContainerInterface::NULL_ON_INVALID_REFERENCE);
        if (NULL !== $securityContext && NULL !== $securityContext->getToken() && $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $loggable = $this->container->get(Gedmo::ListenerLoggable);
            $loggable->setUsername($securityContext->getToken()->getUsername());
        }
    }
}