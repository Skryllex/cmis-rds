<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\MasterTableCode;

class MockQuality implements MockInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\DataFixtures\Fixtures\MockInterface::get()
     */
    public function get()
    {
        return array(
            array(
                'code' => MasterTableCode::Quality . '480',
                'title' => 'SD-480p',
                'description' => "480p is the shorthand name for a family of video display resolutions. The p stands for progressive scan, i.e. non-interlaced." . " The 480 denotes a vertical resolution of 480 pixel high vertically scanning lines, usually with a horizontal resolution of 640 pixels" . " and 4:3 aspect ratio (480 × 4⁄3 = 640) or a horizontal resolution of less than 854 (848 should be used for mod16 compatibility) pixels" . " for an approximate 16:9 aspect ratio (480 × 16⁄9 = 853.33). Since a pixel count must be a whole number," . " in Wide VGA displays it is generally rounded up to 854 to ensure inclusion of the entire image." . " The frames are displayed progressively as opposed to interlaced. 480p was used for many early Plasma televisions." . " Standard definition has always been a 4:3 aspect ratio with a pixel resolution of 640 × 480 pixels. Mini-DV, DVCAM," . " DV footage is recorded at 29.97 frames per second in this standard definition as seen on DV tapes." . " 480p does not qualify as high-definition television (HDTV); it is considered enhanced-definition television (EDTV).",
                'translation' => array(
                    'es' => array(
                        'title' => 'SD-480p',
                        'description' => "480p es el nombre de la taquigrafía para una familia de resoluciones de pantalla de vídeo. La p significa barrido progresivo, es decir, no entrelazado." . "El 480 indica una resolución vertical de 480 píxeles de alta líneas de exploración vertical, por lo general con una resolución horizontal de 640 píxeles" . "Y 4: 3 relación de aspecto (480 × 4/3 = 640) o una resolución horizontal de menos de 854 (848 deben usarse para compatibilidad mod16) píxeles" . "Para un 16 aproximado:. 9 relación de aspecto (480 × 16/9 = 853,33) Dado que un número de píxeles debe ser un número entero" . "En las pantallas VGA anchas en general se redondea a 854 para asegurar la inclusión de toda la imagen." . "Los marcos se muestran progresivamente en lugar de entrelazado. 480p se utilizó durante muchos televisores de plasma primeros." . "Definición estándar ha sido siempre una relación 4: 3 de aspecto con una resolución de píxel de 640 × 480 píxeles Mini-DV, DVCAM,." . "DV imágenes se registra a 29,97 fotogramas por segundo en esta definición estándar como se ve en las cintas DV." . "480p no califica como la televisión de alta definición (HDTV), sino que se considera una mayor definición Televisión (EDTV)."
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Quality . '720',
                'title' => 'HD-720p',
                'description' => "720p (aka HD) is a progressive HDTV signal format with 720 horizontal lines and an aspect ratio (AR) of 16:9 (1.78:1)." . " All major HDTV broadcasting standards (such as SMPTE 292M) include a 720p format which has a resolution of 1280×720;" . " however, there are other formats, including HDV Playback and AVCHD for camcorders, which use 720p images with the standard HDTV resolution." . " The number 720 stands for the 720 horizontal scan lines of image display resolution (also known as 720 pixels of vertical resolution)," . " while the letter p stands for progressive scan (i.e. non-interlaced). When broadcast at 60 frames per second," . " 720p features the highest temporal resolution possible under the ATSC and DVB standards. The term assumes a widescreen aspect ratio of 16:9," . " thus implying a resolution of 1280×720 px (0.9 megapixels). 720i (720 lines interlaced) is an erroneous term found in numerous sources" . " and publications. Typically, it is a typographical error in which the author is referring to the 720p HDTV format." . " However, in some cases it is incorrectly presented as an actual alternative format to 720p." . " No proposed or existing broadcast standard permits 720 interlaced lines in a video frame at any frame rate.",
                'translation' => array(
                    'es' => array(
                        'title' => 'HD-720p',
                        'description' => "720p (también conocido como HD) es un formato de señal HDTV progresiva con 720 líneas horizontales y una relación de aspecto (AR) de 16: 9 (1.78: 1)." . "Todos los principales estándares de emisión HDTV (como SMPTE 292M) incluyen un formato 720p, que tiene una resolución de 1280 × 720;" . "Sin embargo, hay otros formatos, incluidos HDV y AVCHD de reproducción para las videocámaras, que utilizan imágenes 720p con la resolución de HDTV estándar." . "El número 720 representa los 720 líneas de exploración horizontales de resolución de visualización de imágenes (también conocido como 720 píxeles de resolución vertical)," . "Mientras que la letra p significa barrido progresivo (es decir, no entrelazado). Cuando transmitido a 60 cuadros por segundo" . "720p cuenta con la resolución temporal más alta posible bajo los estándares ATSC y DVB El término asume una relación de aspecto de pantalla ancha de 16: 9." . "Lo que implica una resolución de 1280 × 720 píxeles (0,9 megapíxeles). 720i (720 líneas entrelazadas) es un término erróneo encontrado en numerosas fuentes" . "Y publicaciones. Por lo general, es un error tipográfico en la que el autor se refiere al formato HDTV 720p." . "Sin embargo, en algunos casos se presenta incorrectamente como un formato alternativo real 720p." . "No estándar de emisión existente propuesta o permite 720 líneas entrelazadas en un fotograma de vídeo en cualquier frecuencia de imagen."
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Quality . '1080',
                'title' => 'HD-1080p',
                'description' => "1080p (also known as Full HD or FHD and BT.709) is a set of HDTV high-definition video modes characterized" . " by 1080 horizontal lines of vertical resolution and progressive scan, as opposed to interlaced, as is the case with the 1080i display standard." . " The term usually assumes a widescreen aspect ratio of 16:9, implying a resolution of 1920x1080 (2.1 megapixel) often marketed as Full HD.",
                'translation' => array(
                    'es' => array(
                        'title' => 'HD-1080p',
                        'description' => "1080p (también conocida como Full HD o FHD y BT.709) es un conjunto de modos de vídeo de alta definición HDTV caracterizado" . "Por 1080 líneas horizontales de resolución vertical y de exploración progresiva, a diferencia del entrelazado, como es el caso con el estándar de visualización 1080i." . "El término generalmente asume una relación de aspecto de pantalla ancha de 16:9, lo que implica una resolución de 1920x1080 (2.1 megapíxeles) a menudo comercializado como Full HD."
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Quality . '4K',
                'title' => 'UHD-4K',
                'description' => "4K resolution, also called 4K, refers to a display device or content having horizontal resolution on the order of 4,000 pixels." . " Several 4K resolutions exist in the fields of digital television and digital cinematography. In the movie projection industry," . " Digital Cinema Initiatives (DCI) is the dominant 4K standard. A 4K resolution, as defined by Digital Cinema Initiatives," . " is 4096 x 2160 pixels (256:135, approximately a 1.9:1 aspect ratio). This standard is widely respected by the film industry" . " along with all other DCI standards. DCI 4K should not be confused with ultra-high-definition television (UHDTV) AKA UHD-1," . " which has a resolution of 3840 x 2160 (16:9, or approximately a 1.78:1 aspect ratio). Many manufacturers may advertise their products as UHD 4K," . " or simply 4K, when the term 4K is traditionally reserved for the cinematic, DCI resolution. This often causes great confusion among consumers.",
                'translate' => array(
                    'es' => array(
                        'title' => 'UHD-4K',
                        'description' => "Resolución 4K, también llamado 4K, se refiere a un dispositivo de visualización o el contenido que tiene una resolución horizontal del orden de 4.000 píxeles." . "Existen varias resoluciones 4K en el ámbito de la televisión digital y la cinematografía digital. En la industria de la proyección de la película" . "Digital Cinema Initiatives (DCI) es el estándar dominante 4K. Una resolución 4K, según la definición de Digital Cinema Initiatives" . "Es de 4096 x 2160 píxeles (256: 135, aproximadamente a 1.9: 1 relación de aspecto) Esta norma es ampliamente respetado por la industria del cine." . "Junto con todas las demás normas de la DCI. DCI 4K no se debe confundir con la televisión de ultra alta definición (UHDTV) conocido como UHD-1" . ", que tiene una resolución de 3840 x 2160 (16: 9, o aproximadamente un 1.78: 1 relación de aspecto). Muchos fabricantes pueden anunciar sus productos como UHD 4K" . "o simplemente, 4K, cuando el término 4K es tradicionalmente reservado para el cine, la resolución de DCI. A menudo, esto causa gran confusión entre los consumidores."
                    )
                )
            )
        );
    }
}