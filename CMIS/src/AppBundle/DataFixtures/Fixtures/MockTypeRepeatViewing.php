<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\MasterTableCode;

final class MockTypeRepeatViewing implements MockInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\DataFixtures\Fixtures\MockInterface::get()
     */
    public function get()
    {
        return array(
            array(
                'code' => MasterTableCode::TypeRepeatViewing . 'CT',
                'title' => 'Custom',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Personalizado'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::TypeRepeatViewing . 'NV',
                'title' => 'Never',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Nunca'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::TypeRepeatViewing . 'OM',
                'title' => 'Once a month',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Una vez al mes'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::TypeRepeatViewing . 'OW',
                'title' => 'Once a week',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Una vez a la semana'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::TypeRepeatViewing . 'OY',
                'title' => 'Once a year',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Una vez al año'
                    )
                )
            )
        );
    }
}
