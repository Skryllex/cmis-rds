<?php
namespace AppBundle\DataFixtures\Fixtures;

interface MockInterface
{
    /**
     * Get an array data.
     */
    public function get();
}