<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\TypeBroadCastPlatform as ETypeBroadCastPlatform;

final class MockTypeBroadCastPlatform implements MockInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\DataFixtures\Fixtures\MockInterface::get()
     */
    public function get()
    {
        return array(
            array(
                'code' => ETypeBroadCastPlatform::BroadcastTelevisionNetwork,
                'title' => 'Broadcast television network',
                'description' => 'A television network is a telecommunications network for distribution of television program content, whereby a central operation provides programming to many television stations or pay television providers. Until the mid-1980s, television programming in most countries of the world was dominated by a small number of broadcast networks. Many early television networks (such as the BBC, NBC or CBC) evolved from earlier radio networks.',
                'translation' => array(
                    'es' => array(
                        'title' => 'Cadena de televisión',
                        'description' => 'Una cadena de televisión es una red de telecomunicaciones para la distribución de contenido de programa de televisión, por lo que una operación central proporciona la programación de muchas estaciones de televisión o los proveedores de televisión de pago. Hasta mediados de la década de 1980, los programas de televisión en la mayoría de países del mundo estaba dominado por un pequeño número de cadenas de televisión abierta. Muchas cadenas de televisión tempranas (como la BBC, NBC o CBC) evolucionaron a partir de redes de radio anteriores.'
                    )
                )
            )
            ,
            array(
                'code' => ETypeBroadCastPlatform::PayTelevision,
                'title' => 'Pay television',
                'description' => 'Pay television, subscription television, premium television, or premium channels refer to subscription-based television services, usually provided by both analog and digital cable and satellite television, but also increasingly via digital terrestrial and internet television. Subscription television began in the multi-channel transition and transitioned into the post-network era. Some parts of the world, notably in France and the United States, have also offered encrypted analog terrestrial signals available for subscription.',
                'translation' => array(
                    'es' => array(
                        'title' => 'Televisión de pago',
                        'description' => 'Televisión de pago, televisión por suscripción, televisión premium, o canales premium se refieren a los servicios de televisión por suscripción, por lo general prestados por tanto analógica y la televisión por cable y satélite digital, sino también cada vez más a través de la televisión terrestre e internet digital. Televisión por suscripción se inició en la transición de varios canales y la transición a la era posterior a la red. Algunas partes del mundo, sobre todo en Francia y Estados Unidos, también han ofrecido señales terrestres analógicas cifrados disponibles para la suscripción.'
                    )
                )
            ),
            array(
                'code' => ETypeBroadCastPlatform::JointVenture,
                'title' => 'Joint venture',
                'description' => 'A joint venture (JV) is a business agreement in which the parties agree to develop, for a finite time, a new entity and new assets by contributing equity. They exercise control over the enterprise and consequently share revenues, expenses and assets. There are other types of companies such as JV limited by guarantee, joint ventures limited by guarantee with partners holding shares.',
                'translation' => array(
                    'es' => array(
                        'title' => 'Empresa conjunta',
                        'description' => 'Una empresa conjunta (JV) es un acuerdo comercial en el que las partes se comprometen a desarrollar, durante un tiempo finito, una nueva entidad y los nuevos activos, contribuyendo equidad. Ejercen control sobre la empresa y por lo tanto comparten los ingresos, gastos y bienes. Hay otros tipos de empresas como JV limitadas por garantía, empresas conjuntas limitadas por garantía con socios que poseen acciones.'
                    )
                )
            ),
            array(
                'code' => ETypeBroadCastPlatform::Subsidiary,
                'title' => 'Subsidiary',
                'description' => 'A subsidiary, subsidiary company or daughter company is a company that is owned or controlled by another company, which is called the parent company, parent, or holding company. The subsidiary can be a company, corporation, or limited liability company. In some cases it is a government or state-owned enterprise. In the United States railroad industry, an operating subsidiary is a company that is a subsidiary but operates with its own identity, locomotives and rolling stock. In contrast, a non-operating subsidiary would exist on paper only (i.e., stocks, bonds, articles of incorporation) and would use the identity and rolling stock of the parent company.',
                'translation' => array(
                    'es' => array(
                        'title' => 'Subsidiaria',
                        'description' => 'Una subsidiaria, empresa subsidiaria o filial es una empresa que es propiedad o controlada por otra empresa, que se llama la empresa matriz, padre o sociedad de cartera. La filial puede ser una empresa, corporación o sociedad de responsabilidad limitada. En algunos casos se trata de un gobierno o empresa estatal. En la industria del ferrocarril de Estados Unidos, una subsidiaria operativa es una empresa que es una filial, pero opera con su propia identidad, locomotoras y material rodante. Por el contrario, existiría una filial no operativa en el papel solamente (es decir, acciones, bonos, escritura de constitución) y utilizaría la identidad y el material rodante de la empresa matriz.'
                    )
                )
            ),
            array(
                'code' => ETypeBroadCastPlatform::VideoOnDemand,
                'title' => 'Video on demand (VOD)',
                'description' => 'Video on demand (VOD) are systems which allow users to select and watch/listen to video or audio content when they choose to, rather than having to watch at a specific broadcast time. IPTV technology is often used to bring video on demand to televisions and personal computers.',
                'translation' => array(
                    'es' => array(
                        'title' => 'Vídeo bajo demanda',
                        'description' => 'Video bajo demanda (VOD) son sistemas que permiten a los usuarios seleccionar y ver / escuchar vídeo o contenido de audio, cuando decidieron, en lugar de tener que ver a una hora de emisión específica. Tecnología IPTV se utiliza a menudo para llevar el vídeo bajo demanda de televisores y ordenadores personales.'
                    )
                )
            )
        );
    }
}