<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\TypeCast as EnumTypeCast;

final class MockTypeCast implements MockInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\DataFixtures\Fixtures\MockInterface::get()
     */
    public function get()
    {
        return array(
            array(
                'code' => EnumTypeCast::ActorActress,
                'title' => 'Actor/Actress',
                'description' => "An actor (actress for a female; see terminology) is a person portraying a character in a dramatic or comic production;" . " he or she performs in film, television, theatre, radio, commercials or music videos. Actor, ὑποκριτής (hypokrites)," . " literally means one who interprets; an actor, then, is one who interprets a dramatic character." . " Method acting is an approach in which the actor identifies with the portrayed character by recalling emotions or reactions from his or her own life." . " Presentational acting refers to a relationship between actor and audience, whether by direct address or indirectly by specific use of language," . " looks, gestures or other signs indicating that the character or actor is aware of the audience's presence." . " In representational acting, actors want to make us 'believe' they are the character; they pretend." . " Formerly, in some societies, only men could become actors, and women's roles were generally played by men or boys." . " In modern times, women occasionally played the roles of prepubescent boys.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Actor/Actriz',
                        'description' => "Un actor (actriz para una mujer; ver terminología) es una persona que retrata a un personaje en una producción dramática o cómica;" . "Él o ella realiza en cine, televisión, teatro, radio, anuncios publicitarios o videos musicales. Actor, ὑποκριτής (hypokrites)" . "Literalmente significa aquel que interpreta; un actor, entonces, es el que interpreta a un personaje dramático." . "Método de actuación es un enfoque en el que el actor se identifica con el personaje interpretado por recordar las emociones o reacciones de su propia vida." . "Actuando presentacional refiere a una relación entre el actor y el público, ya sea por la dirección directa o indirectamente por el uso específico del lenguaje" . "Miradas, gestos u otros signos que indican que el personaje o actor es consciente de la presencia de la audiencia." . "En la actuación de representación, los actores quieren hacernos 'creemos' que son el carácter, sino que pretenden" . "Antes, en algunas sociedades, sólo los hombres pueden convertirse en actores y roles de las mujeres fueron jugado generalmente por hombres o los niños." . "En los tiempos modernos, las mujeres de vez en cuando jugaron el papel de los niños pre-púberes."
                    )
                )
            ),
            array(
                'code' => EnumTypeCast::Director,
                'title' => 'Director',
                'description' => "A director is a person who directs the making of a film. Generally, a director controls a film's artistic and dramatic aspects," . " and visualizes the script while guiding the technical crew and actors in the fulfillment of that vision." . " The director has a key role in choosing the cast members, production design, and the creative aspects of filmmaking." . " Under European Union law, the director is viewed as the author of the film." . " The film director gives direction to the cast and crew and creates an overall vision through which a film eventually becomes realized." . " Directors need to be able to mediate differences in creative visions and stay in the boundaries of the film's budget." . " There are many pathways to becoming a film director. Some film directors started as screenwriters, cinematographers, film editors or actors." . " Other film directors have attended a film school. Directors use different approaches." . " Some outline a general plotline and let the actors improvise dialogue, while others control every aspect," . " and demand that the actors and crew follow instructions precisely." . " Some directors also write their own screenplays or collaborate on screenplays with long-standing writing partners." . " Some directors edit or appear in their films, or compose the music score for their films.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Director/a',
                        'description' => "Un director es una persona que dirige la realización de una película. Por lo general, un director controla aspectos artísticos y dramáticos de una película" . "Y visualiza el guión mientras guía el equipo técnico y los actores en el cumplimiento de esa visión." . "El director tiene un papel clave en la elección de los miembros del reparto, diseño de producción, y los aspectos creativos de cine." . "En virtud del derecho de la Unión Europea, el director es visto como el autor de la película." . "El director de cine da dirección al reparto y del equipo y crea una visión de conjunto a través del cual una película finalmente se convierte en realidad." . "Los directores tienen que ser capaces de mediar en las diferencias de visiones creativas y permanecer en los límites del presupuesto de la película." . "Hay muchos caminos para convertirse en un director de cine. Algunos directores de cine comenzó como guionistas, directores de fotografía, editores de cine o actores." . "Otros directores de cine han asistido a una escuela de cine. Directores utilizan diferentes enfoques." . "Algunos esbozo una trama general y dejar que los actores improvisan el diálogo, mientras que otros controlan todos los aspectos" . "Y exigir que los actores y las instrucciones complementarias tripulación con precisión." . "Algunos directores también escriben sus propios guiones o colaboran en guiones con larga data socios de la escritura." . "Algunos directores de editar o aparecer en sus películas, o componer la banda sonora de sus películas."
                    )
                )
            ),
            array(
                'code' => EnumTypeCast::ExecutiveProducer,
                'title' => 'Executive producer',
                'description' => "The Executive Producer addresses the finances in that he pitches films to the studios," . " but upon acceptance he may focus on business matters, such as budgets and contracts.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Productor/a ejecutivo/a',
                        'description' => "El Productor Ejecutivo se ocupa de las finanzas en que él lanza películas a los estudios" . "Pero después de la aceptación que puede centrarse en asuntos de negocios, tales como presupuestos y contratos."
                    )
                )
            ),
            array(
                'code' => EnumTypeCast::Producer,
                'title' => 'Producer',
                'description' => "Film producers fill a variety of roles depending upon the type of producer." . " Either employed by a production company or independent, producers plan and coordinate various aspects of film production," . " such as selecting script, coordinating writing, directing and editing, and arranging financing." . " The average Hollywood film made in 2013 had just over 10 producer credits (3.2 producers, 4.4 executive producers, 1.2 co-producers," . " 0.8 associate producers and 0.5 other types of producer)." . " During the 'discovery stage' the producer has to find and acknowledge promising material." . " Then, unless the film is supposed to be based on an original script, the producer has to find an appropriate screenwriter." . " For various reasons, producers cannot always personally supervise all of the production." . " As such, the main producer will appoint executive producers, line producers or unit production managers who represent the main producer's interests." . " Among other things, the producer has the last word on whether sounds or music have to be changed or scenes have to be cut" . " and they are in charge of selling the film or arranging distribution rights.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Productor/a',
                        'description' => "Los productores de cine se llenan una variedad de funciones dependiendo del tipo de productor." . "De cualquier empleado de una empresa de producción o plan independiente, productores y coordinar diversos aspectos de la producción de la película," . "Tales como la selección de la escritura, la coordinación de la escritura, dirección y edición, y la organización de la financiación." . "La película media de Hollywood realizada en 2013 tenía poco más de 10 créditos como productor (3.2 productores, productores ejecutivos, 4.4 1.2 coproductores" . "0.8 productores asociados y otros 0,5 tipos de productor)." . "Durante la 'etapa de descubrimiento' el productor tiene que encontrar y reconocer material prometedor. " . "Entonces, a menos que se supone que la película se basará en un guión original, el productor tiene que encontrar un guionista adecuado." . "Por diversas razones, los productores pueden no siempre supervisar personalmente toda la producción." . "Como tal, el principal productor nombrará productores ejecutivos, productores de línea o gerentes de producción por unidad que representan los intereses del productor principal." . "Entre otras cosas, el productor tiene la última palabra sobre si los sonidos o la música tiene que ser cambiado o escenas que cortar" . "Y ellos se encargan de la venta de la película o la organización de derechos de distribución."
                    )
                )
            ),
            array(
                'code' => EnumTypeCast::Showrunner,
                'title' => 'Showrunner',
                'description' => "In the United States television industry, a showrunner is a person who is responsible for the day-to-day operation" . " of a television series—although usually such persons are credited as executive producers." . " The term is also occasionally applied to people in the television industries of other countries." . " The showrunner is at the opposite end of the staff hierarchy from runners, who are the most junior members of the production team," . " although showrunners are sometimes (often humorously) called runners for short." . " A showrunner's duties often combine those traditionally assigned to the writer, executive producer and script editor." . " Unlike films, where directors are in creative control of a production, in episodic television, the showrunner outranks the director.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Showrunner',
                        'description' => "En la industria de la televisión de Estados Unidos, un productor ejecutivo es una persona que es responsable de la operación del día a día" . "De una televisión a pesar de serie por lo general estas personas se acreditan como productores ejecutivos." . "El término también se aplica a la gente de vez en cuando en las industrias de la televisión de otros países." . "El productor ejecutivo es en el extremo opuesto de la jerarquía del personal de los corredores, que son los miembros más jóvenes del equipo de producción" . "Aunque showrunners son a veces (a menudo con humor) llamado corredores para abreviar." . "Los deberes de un productor ejecutivo menudo combinan los tradicionalmente asignado al escritor, productor ejecutivo y editor de secuencia de comandos." . "A diferencia de las películas, donde los directores se encuentran en el control creativo de una producción, en series de televisión, el productor ejecutivo supera el director."
                    )
                )
            ),
            array(
                'code' => EnumTypeCast::Screenwriter,
                'title' => 'Screenwriter',
                'description' => "A screenplay writer, screenwriter for short, scriptwriter or scenarist is a writer who practices the craft of screenwriting," . " writing screenplays on which mass media such as films, television programs, comics or video games are based.",
                'translation' => array(
                    'es' => array(
                        'title' => 'Guionista',
                        'description' => "Un escritor guión, guionista, para abreviar, guionista o guionista es un escritor que practica el arte de la escritura de guiones" . "Escribir guiones en los que se basan los medios de comunicación, tales como películas, programas de televisión, cómics o videojuegos."
                    )
                )
            )
        );
    }
}