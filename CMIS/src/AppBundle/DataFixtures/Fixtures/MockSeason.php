<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\MasterTableCode;

final class MockSeason implements MockInterface
{

    public function get()
    {
        return array(
            array(
                'code' => MasterTableCode::Season . 'S1',
                'title' => 'Season 1',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 1'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S2',
                'title' => 'Season 2',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 2'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S3',
                'title' => 'Season 3',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 3'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S4',
                'title' => 'Season 4',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 4'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S5',
                'title' => 'Season 5',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 5'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S6',
                'title' => 'Season 6',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 6'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S7',
                'title' => 'Season 7',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 7'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S8',
                'title' => 'Season 8',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 8'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S9',
                'title' => 'Season 9',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 9'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S10',
                'title' => 'Season 10',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 10'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S11',
                'title' => 'Season 11',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 11'
                    )
                )
            ),
            array(
                'code' => MasterTableCode::Season . 'S12',
                'title' => 'Season 12',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Temporada 12'
                    )
                )
            )
        );
    }
}