<?php
namespace AppBundle\DataFixtures\Fixtures;

use AppBundle\DataFixtures\Fixtures\MockInterface;
use AppBundle\Enum\StateSeries as EnumStateSeries;

final class MockStateSeries implements MockInterface
{

    public function get()
    {
        return array(
            array(
                'code' => EnumStateSeries::InBroadcasting,
                'title' => 'In broadcasting',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'En emisión'
                    )
                )
            ),
            array(
                'code' => EnumStateSeries::WaitingNewSeason,
                'title' => 'Waiting new season',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'A la espera de una nueva temporada'
                    )
                )
            ),
            array(
                'code' => EnumStateSeries::CanceledBroadcasting,
                'title' => 'Canceled broadcasting',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Emisión cancelada'
                    )
                )
            ),
            array(
                'code' => EnumStateSeries::FinishedBroadcasting,
                'title' => 'Finished broadcasting',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Emisión finalizada'
                    )
                )
            ),
            array(
                'code' => EnumStateSeries::PendingBroadcasting,
                'title' => 'Pending broadcasting',
                'description' => '',
                'translation' => array(
                    'es' => array(
                        'title' => 'Pendiente de emisión'
                    )
                )
            )
        );
    }
}