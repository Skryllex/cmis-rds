<?php
/**
 * @file
 * DataFixture Groups.
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Fixtures\MockGroup;
use AppBundle\Entity\Group;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class Groups implements FixtureInterface
{

    /**
     * Non-PHPdoc
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_group = new MockGroup();
        $groups = $mock_group->get();

        if (! empty($groups)) {
            foreach ($groups as $group) {
                $g = new Group($group['name'], $group['roles']);
                $g->setCode($group['code']);
                $manager->persist($g);
            }
            $manager->flush();
        }
    }
}