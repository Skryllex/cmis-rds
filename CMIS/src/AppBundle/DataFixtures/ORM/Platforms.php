<?php
/**
 * @file
 * DataFixture StateSeries.
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Fixtures\MockTypeBroadCastPlatform;
use AppBundle\Enum\Service;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Platforms implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     *
     * {@inheritdoc}
     *
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $mock_type_broadcast_platform = new MockTypeBroadCastPlatform();
        $platforms = $mock_type_broadcast_platform->get();

        if (! empty($platforms)) {
            $data = array();
            foreach ($platforms as $platform) {
                $builder = $this->container->get(Service::BuilderTypeBroadcastPlatformBuilder);
                $object = $builder->build($platform['code'], $platform['title'], $platform['description']);
                if (! empty($platform['translation']) && method_exists($builder, 'setTranslation')) {
                    $object = $builder->setTranslation($object, $platform['translation']);
                }
                $data[] = $object;
            }
            $persister = new \Nelmio\Alice\Persister\Doctrine($manager);
            $persister->persist($data);
        }
    }
}