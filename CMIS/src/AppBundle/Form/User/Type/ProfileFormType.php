<?php
/**
 * @file
 * Overrides Profile Form.
 */

namespace AppBundle\Form\User\Type;

use Sonata\UserBundle\Form\Type\ProfileType as SonataUserProfileFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ProfileFormType extends SonataUserProfileFormType
{

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('address', 'text', array(
            'label' => 'Address',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 100
                ))
            )
        ))
            ->add('phone')
            ->add('city', 'text', array(
            'label' => 'City',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 50
                ))
            )
        ))
            ->add('country', 'country', array(
            'label' => 'Country',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 10
                ))
            )
        ));
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::getParent()
     */
    public function getParent()
    {
        return 'fos_user_profile';
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::getBlockPrefix()
     */
    public function getBlockPrefix()
    {
        return 'cmis_user_profile';
    }
}