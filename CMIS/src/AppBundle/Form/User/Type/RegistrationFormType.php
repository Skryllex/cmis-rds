<?php
/**
 * @file
 * Overrides Registration Form.
 */

namespace AppBundle\Form\User\Type;

use Sonata\UserBundle\Form\Type\RegistrationFormType as SonataUserRegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class RegistrationFormType extends SonataUserRegistrationFormType
{

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime();

        $builder->add('firstname', 'text', array(
            'label' => ucfirst('firstname')
        ))
            ->add('lastname', 'text', array(
            'label' => ucfirst('lastname'),
            'required' => FALSE
        ))
            ->add('dateOfBirth', 'sonata_type_date_picker', array(
            'years' => range(1900, $now->format('Y')),
            'dp_min_date' => '1-1-1900',
            'dp_max_date' => $now->format('c'),
            'required' => FALSE
        ))
            ->add('gender', 'sonata_user_gender', array(
            'required' => TRUE,
            'translation_domain' => 'SonataUserBundle'
        ))
            ->add('address', 'text', array(
            'label' => 'Address',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 100
                ))
            )
        ))
            ->add('phone')
            ->add('city', 'text', array(
            'label' => 'City',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 50
                ))
            )
        ))
            ->add('country', 'country', array(
            'label' => 'Country',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 10
                ))
            )
        ));
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::getParent()
     */
    public function getParent()
    {
        return 'fos_user_registration';
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Form\AbstractType::getBlockPrefix()
     */
    public function getBlockPrefix()
    {
        return 'cmis_user_registration';
    }
}