<?php
/**
 * @file
 * Code validator.
 */
namespace AppBundle\Validator\Entity\Constraint\BasicMasterTable;

use AppBundle\Enum\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class CodeValidator extends ConstraintValidator
{

    protected $container;

    /**
     * Default constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Validator\ConstraintValidatorInterface::validate()
     */
    public function validate($value, Constraint $constraint)
    {
        if (! $constraint instanceof Code) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\Code');
        }

        $em = $this->container->get(Service::DoctrineIdentifier)->getManager();
        $exists = $em->getRepository($this->bundle)->findOneBy(array(
            'code' => $value
        ));
        if (isset($exists)) {
            $this->context->addViolationAt('code', $constraint->message, array(), null);
        }
    }

    /**
     * Returns the name of the class that validates this constraint.
     *
     * @return string
     */
    public function validateBy()
    {
        return 'code_exists';
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Validator\Constraint::getTargets()
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}