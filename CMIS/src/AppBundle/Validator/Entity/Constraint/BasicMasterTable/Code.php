<?php
namespace AppBundle\Validator\Entity\Constraint\BasicMasterTable;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "PROPERTY", "ANNOTATION"})
 */
final class Code extends Constraint
{

    /**
     * Default message.
     *
     * @var string
     */
    public $message = 'The code value already exists.';

    /**
     * Bundle.
     *
     * @var string
     */
    public $bundle;

    /**
     * Non-PHPdoc.
     *
     * @see \Symfony\Component\Validator\Constraint::getTargets()
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}