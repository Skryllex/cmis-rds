<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Builder\User;

use AppBundle\Enum\Gender;
use AppBundle\Enum\StateUser;
use SylrSyksSoftSymfony\CoreBundle\Builder\TranslatableBuilderTrait;

class BuilderUserBuilder
{
    use TranslatableBuilderTrait;

    /**
     * Content media builder.
     *
     * @param string $username
     *            User name.
     * @param string $email
     *            Email.
     * @param string $plainPassword
     *            Plain password.
     * @param string $firstname
     *            Firstname.
     * @param array $lastname
     *            Lastname.
     * @param array $birth_date
     *            Birthdate.
     * @param array $gender
     *            Gender.
     * @param string $address
     *            Address.
     * @param string $postcode
     *            Postcode.
     * @param string $city
     *            City.
     * @param string $country
     *            Country.
     * @param string $website
     *            Website.
     * @param string $state
     *            State.
     * @param array $roles
     *            Roles.
     * @param array $groups
     *            Groups.
     * @param array $images
     *            Images.
     */
    public function build($username, $email, $plainPassword, $firstname, $lastname, $birth_date = NULL, $gender = Gender::__default, $address = NULL,
        $postcode = NULL, $city = NULL, $country = NULL, $website = NULL, $state = StateUser::__default, $roles = array(), $groups = array(), $images = array())
    {
        $this->builder->create($username, $email, $plainPassword, $firstname, $lastname, $birth_date, $gender, $address, $postcode, $city, $country, $website, $state, $roles);

        if (! empty($groups)) {
            $this->builder->addGroups($groups);
        }

        if (! empty($images)) {
            $this->builder->addImages($images);
        }

        return $this->builder->get();
    }
}