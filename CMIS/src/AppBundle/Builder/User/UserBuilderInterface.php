<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Builder\User;

use AppBundle\Enum\Gender;
use AppBundle\Enum\StateUser;
use SylrSyksSoftSymfony\CoreBundle\Builder\BuilderInterface;

interface UserBuilderInterface extends BuilderInterface
{

    /**
     * Create a document \AppBundle\Document\User.
     *
     * @param string $username
     *            User name.
     * @param string $email
     *            Email.
     * @param string $plainPassword
     *            Plain password.
     * @param string $firstname
     *            Firstname.
     * @param string $lastname
     *            Lastname.
     * @param \DateTime $birth_date
     *            Birthdate.
     * @param string $gender
     *            Gender.
     * @param string $address
     *            Address.
     * @param string $postcode
     *            Postcode.
     * @param string $city
     *            City.
     * @param string $country
     *            Country.
     * @param string $website
     *            Website.
     * @param string $state
     *            State.
     * @param array $roles
     *            Roles.
     */
    public function create($username, $email, $plainPassword, $firstname, $lastname, \DateTime $birth_date = NULL, $gender = Gender::__default,
        $address = NULL, $postcode = NULL, $city = NULL, $country = NULL, $website = NULL, $state = StateUser::__default, array $roles = array());

    /**
     * Add groups.
     */
    public function addGroups($groups);
}