<?php
/**
 * @file
 * User Builder.
 */
namespace AppBundle\Builder\User;

use AppBundle\Entity\Group;
use AppBundle\Enum\Gender;
use AppBundle\Enum\StateUser;
use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\UserBundle\Model\UserManager;
use SylrSyksSoftSymfony\CoreBundle\Builder\Entity\EntityBuilder;

class UserBuilder extends EntityBuilder implements UserBuilderInterface
{

    /**
     *
     * @var \FOS\UserBundle\Model\UserManager
     */
    private $userManager;

    /**
     * Default constructor.
     *
     * @param ManagerRegistry $manager
     *            Manager.
     * @param UserManager $userManager
     *            User manager.
     * @param string $className
     *            Class name.
     */
    public function __construct(ManagerRegistry $manager, UserManager $userManager, $className)
    {
        parent::__construct($manager, $className);
        $this->userManager = $userManager;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\User\UserBuilderInterface::create()
     */
    public function create($username, $email, $plainPassword, $firstname, $lastname, \DateTime $birth_date = NULL, $gender = Gender::__default,
        $address = NULL, $postcode = NULL, $city = NULL, $country = NULL, $website = NULL, $state = StateUser::__default, array $roles = array())
    {
        $this->object->setUsername($username)
            ->setEmail($email)
            ->setPlainPassword($plainPassword)
            ->setFirstname($firstname)
            ->setLastname($lastname)
            ->setDateOfBirth($birth_date)
            ->setGender($gender)
            ->setAddress($address)
            ->setPostcode($postcode)
            ->setCity($city)
            ->setCountry($country)
            ->setWebsite($website)
            ->setEnabled($state);

        if (! empty($roles)) {
            $this->object->setRealRoles($roles);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\User\UserBuilderInterface::addGroups()
     */
    public function addGroups($groups)
    {
        if (! empty($groups)) {
            foreach ($groups as $group) {
                $conditions = array(
                    'name' => $group['code']
                );
                $g = $this->manager->getRepository(Group::getBundle())->findOneBy($conditions);
                if (isset($g)) {
                    $this->object->addGroup($g);
                }
            }
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \AppBundle\Builder\User\UserBuilderInterface::addImages()
     */
    public function addImages($images)
    {
        // TODO: Auto-generated method stub
    }
}