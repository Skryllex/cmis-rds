<?php
/**
 * @file
 * Manage the entity User.
 */
namespace AppBundle\Admin\Entity;

use AppBundle\Enum\Role;
use AppBundle\Twig\Admin\Enum\SonataGrid as ThemeGrid;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\UserBundle\Admin\Entity\UserAdmin as SonataUserAdmin;
use Symfony\Component\Validator\Constraints as Assert;

final class UserAdmin extends SonataUserAdmin
{

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\UserBundle\Admin\Model\UserAdmin::configureDatagridFilters()
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper->add('username')
            ->add('email')
            ->add('groups');
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\UserBundle\Admin\Model\UserAdmin::configureListFields()
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('username')
            ->add('email')
            ->add('groups');

        $options = array();
        if ($this->isGranted(Role::SuperAdministrator)) {
            $options = array(
                'editable' => TRUE
            );
        }

        $listMapper->add('enabled', NULL, $options)->add('locked', NULL, $options);

        if ($this->isGranted(Role::SuperAdministrator)) {
            $listMapper->add('lastLogin');
        }

        if ($this->isGranted(Role::AllowedSwitch)) {
            $listMapper->add('impersonating', 'string', array(
                'template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig'
            ));
        }

        if ($this->isGranted(Role::SuperAdministrator)) {
            $listMapper->add('_action', 'actions', array(
                'label' => 'Actions',
                'actions' => array(
                    'view' => array(),
                    'edit' => array(),
                    'delete' => array()
                )
            ));
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\UserBundle\Admin\Model\UserAdmin::configureShowFields()
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->with('General', array(
            'class' => ThemeGrid::SixColumns
        ))
            ->add('username')
            ->add('email')
            ->end()
            ->with('Groups', array(
            'class' => ThemeGrid::SixColumns
        ))
            ->add('groups')
            ->end()
            ->with('Profile', array(
            'class' => ThemeGrid::TwelveColumns
        ))
            ->add('firstname')
            ->add('lastname')
            ->add('dateOfBirth')
            ->add('gender')
            ->add('address', NULL, array(
            'label' => 'Address'
        ))
            ->add('phone')
            ->add('postcode', NULL, array(
            'label' => 'Postcode'
        ))
            ->add('city', NULL, array(
            'label' => 'City'
        ))
            ->add('country', NULL, array(
            'label' => 'Country'
        ))
            ->add('website')
            ->add('locale')
            ->add('timezone')
            ->end()
            ->with('Facebook', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->add('facebookUid')
            ->add('facebookName')
            ->end()
            ->with('Google +', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->add('gplusName')
            ->add('gplusUid')
            ->end()
            ->with('Twitter', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->add('twitterUid')
            ->add('twitterName')
            ->end()
            ->with('Keys', array(
            'class' => ThemeGrid::TwelveColumns
        ))
            ->add('token')
            ->add('twoStepVerificationCode')
            ->end();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\UserBundle\Admin\Model\UserAdmin::configureFormFields()
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper->tab('User')
            ->with('General', array(
            'class' => ThemeGrid::TwelveColumns
        ))
            ->end()
            ->with('Profile', array(
            'class' => ThemeGrid::TwelveColumns
        ))
            ->end()
            ->end()
            ->tab('User Images')
            ->with('Images', array(
            'class' => ThemeGrid::TwelveColumns
        ))
            ->end()
            ->end()
            ->tab('Social')
            ->with('Facebook', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->end()
            ->with('Google +', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->end()
            ->with('Twitter', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->end()
            ->end()
            ->tab('Security')
            ->with('Status', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->end()
            ->with('Groups', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->end()
            ->with('Keys', array(
            'class' => ThemeGrid::FourColumns
        ))
            ->end()
            ->with('Roles', array(
            'class' => ThemeGrid::TwelveColumns
        ))
            ->end()
            ->end();

        $now = new \DateTime();

        $formMapper->tab('User')
            ->with('General')
            ->add('username')
            ->add('email')
            ->add('plainPassword', 'text', array(
            'required' => (! $this->getSubject() || is_null($this->getSubject()
                ->getId()))
        ))
            ->end()
            ->with('Profile')
            ->add('firstname', NULL, array(
            'required' => FALSE
        ))
            ->add('lastname', NULL, array(
            'required' => FALSE
        ))
            ->add('dateOfBirth', 'sonata_type_date_picker', array(
            'years' => range(1900, $now->format('Y')),
            'dp_min_date' => '1-1-1900',
            'dp_max_date' => $now->format('c'),
            'required' => FALSE
        ))
            ->add('gender', 'sonata_user_gender', array(
            'required' => TRUE,
            'translation_domain' => $this->getTranslationDomain()
        ))
            ->add('address', 'text', array(
            'label' => 'Address',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 100
                ))
            )
        ))
            ->add('phoneNumber', 'tel', array(
            'label' => 'Phone',
            'required' => FALSE,
            'widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE,
            'country_choices' => array(
                'GB',
                'DE',
                'FR',
                'ES'
            ),
            'preferred_country_choices' => array(
                'GB',
                'DE',
                'FR',
                'ES'
            ),
            'constraints' => array(
                new AssertPhoneNumber()
            )
        ))
            ->add('postcode', 'text', array(
            'label' => 'Postcode',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 10
                ))
            )
        ))
            ->add('city', 'text', array(
            'label' => 'City',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 50
                ))
            )
        ))
            ->add('country', 'country', array(
            'label' => 'Country',
            'required' => FALSE,
            'constraints' => array(
                new Assert\Length(array(
                    'max' => 10
                ))
            )
        ))
            ->add('website', 'url', array(
            'required' => FALSE
        ))
            ->add('locale', 'locale', array(
            'required' => FALSE
        ))
            ->add('timezone', 'timezone', array(
            'required' => FALSE
        ))
            ->end()
            ->end();

        $formMapper->tab('User Images')
            ->with('Images')
            ->add('images', 'sonata_type_collection', array(
            'label' => 'Images',
            'required' => TRUE,
            'type_options' => array(
                'delete' => TRUE
            ),
            'by_reference' => FALSE,
            'mapped' => TRUE
        ), array(
            'edit' => 'inline',
            'inline' => 'standard',
            'link_parameters' => array(
                'context' => 'user',
                'provider' => 'sonata.media.provider.image'
            )
        ))
            ->end()
            ->end();

        $formMapper->tab('Social')
            ->with('Facebook')
            ->add('facebookUid', NULL, array(
            'required' => FALSE
        ))
            ->add('facebookName', NULL, array(
            'required' => FALSE
        ))
            ->end()
            ->with('Google +')
            ->add('gplusUid', NULL, array(
            'required' => FALSE
        ))
            ->add('gplusName', NULL, array(
            'required' => FALSE
        ))
            ->end()
            ->with('Twitter')
            ->add('twitterUid', NULL, array(
            'required' => FALSE
        ))
            ->add('twitterName', NULL, array(
            'required' => FALSE
        ))
            ->end()
            ->end();

        if ($this->getSubject() && $this->getSubject()->hasRole(Role::SuperAdministrator)) {
            $formMapper->tab('Security')
                ->with('Status')
                ->add('locked', NULL, array(
                'required' => FALSE
            ))
                ->add('expired', NULL, array(
                'required' => FALSE
            ))
                ->add('enabled', NULL, array(
                'required' => FALSE
            ))
                ->add('credentialsExpired', NULL, array(
                'required' => FALSE
            ))
                ->end()
                ->with('Groups')
                ->add('groups', 'sonata_type_model', array(
                'expanded' => TRUE,
                'multiple' => TRUE,
                'required' => FALSE
            ))
                ->end()
                ->with('Roles')
                ->add('realRoles', 'sonata_security_roles', array(
                'label' => 'form.label_roles',
                'expanded' => TRUE,
                'multiple' => TRUE,
                'required' => FALSE
            ))
                ->end()
                ->end();
        }

        $formMapper->tab('Security')
            ->with('Keys')
            ->add('token', NULL, array(
            'required' => FALSE
        ))
            ->add('twoStepVerificationCode', NULL, array(
            'required' => FALSE
        ))
            ->end()
            ->end();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\AdminBundle\Admin\Admin::validate($errorElement, $object)
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $exists_username = $this->modelManager->findOneBy($this->getClass(), array(
            'username' => $object->getUsername()
        ));

        if (isset($exists_username)) {
            $errorElement->with('username')
                ->addViolation('The username value already exist.')
                ->end();
        }

        $exists_email = $this->modelManager->findOneBy($this->getClass(), array(
            'email' => $object->getEmail()
        ));

        if (isset($exists_email)) {
            $errorElement->with('email')
                ->addViolation('The email value already exist.')
                ->end();
        }
    }
}