<?php
/**
 * @file
 * Manage the entity Season.
 */
namespace AppBundle\Admin\Entity;

use SylrSyksSoftSymfony\CoreBundle\Admin\AbstractMasterTableAdmin;

final class SeasonAdmin extends AbstractMasterTableAdmin
{
    /**
     * Base pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = self::BASE_ROUTE_PATTERN . '/season';
}