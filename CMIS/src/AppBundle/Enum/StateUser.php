<?php
/**
 * @file
 *
 * User Enums.
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class StateUser extends Enum
{

    const __default = self::Enabled;

    const Enabled = 1;

    const Disabled = 0;
}
