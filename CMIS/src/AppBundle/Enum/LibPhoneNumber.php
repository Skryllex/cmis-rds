<?php
/**
 * @file
 * Enum with services for LibPhoneNumber.
 *
 * @link https://github.com/misd-service-development/phone-number-bundle
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

class LibPhoneNumber extends Enum
{

    const __default = self::PhoneNumberUtil;

    const PhoneNumberUtil = 'libphonenumber.phone_number_util';

    const PhoneNumberOfflineGeocoder = 'libphonenumber.phone_number_offline_geocoder';

    const PhoneNumberToCarrierMapper = 'libphonenumber.phone_number_to_carrier_mapper';

    const PhoneNumberToTimeZonesMapper = 'libphonenumber.phone_number_to_time_zones_mapper';

    const ShortNumberInfo = 'libphonenumber.short_number_info';
}