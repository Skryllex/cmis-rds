<?php
/**
 * @file
 *
 * TypeCast Enums.
 *
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class TypeCast extends Enum
{

    const __default = self::ActorActress;

    const ActorActress = MasterTableCode::TypeCast . 'AC';

    const Director = MasterTableCode::TypeCast . 'DC';

    const ExecutiveProducer = MasterTableCode::TypeCast . 'EP';

    const Producer = MasterTableCode::TypeCast . 'PC';

    const Showrunner = MasterTableCode::TypeCast . 'SH';

    const Screenwriter = MasterTableCode::TypeCast . 'SW';
}