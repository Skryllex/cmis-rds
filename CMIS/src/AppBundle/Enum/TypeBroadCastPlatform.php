<?php
/**
 * @file
 *
 * TypeBroadCastPlatform Enums.
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class TypeBroadCastPlatform extends Enum
{

    const __default = self::BroadcastTelevisionNetwork;

    const BroadcastTelevisionNetwork = MasterTableCode::TypeBroadcastPlatform . 'BTN';

    const JointVenture = MasterTableCode::TypeBroadcastPlatform . 'JV';

    const PayTelevision = MasterTableCode::TypeBroadcastPlatform . 'PT';

    const Subsidiary = MasterTableCode::TypeBroadcastPlatform . 'SB';

    const VideoOnDemand = MasterTableCode::TypeBroadcastPlatform . 'VOD';
}