<?php
/**
 * @file
 * Common Enums.
 */
namespace AppBundle\Enum;

use MyCLabs\Enum\Enum;

final class Service extends Enum
{

    const DoctrineIdentifier = 'doctrine';

    const SecurityContextIdentifier = 'security.context';

    const UserBuilder = 'cmis.builder.user';

    const BuilderUserBuilder = 'cmis.builder.user.builder';

    const TypeCastBuilder = 'cmis.builder.master_table.type_cast';

    const BuilderTypeCastBuilder = 'cmis.builder.master_table.builder.type_cast';

    const BuilderGenreBuilder = 'cmis.builder.master_table.builder.genre';

    const BuilderTypeBroadcastPlatformBuilder = 'cmis.builder.master_table.builder.type_broadcast_platform';

    const BuilderTypeRepeatViewingBuilder = 'cmis.builder.master_table.builder.type_repeat_viewing';

    const BuilderQualityBuilder = 'cmis.builder.master_table.builder.quality';

    const BuilderSeasonBuilder = 'cmis.builder.master_table.builder.season';

    const BuilderStateSeriesBuilder = 'cmis.builder.master_table.builder.state_series';

    const UserManager = 'cmis.manager.user';
}
