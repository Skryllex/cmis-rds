<?php
/**
 * @file
 * Entity Movie.
 */
namespace AppBundle\Entity;

use AppBundle\Entity\Common\AbstractContentMedia;
use Doctrine\ORM\Mapping as ORM;

/**
 * Movie
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="MOVIE_IDX", columns={"year", "title"}),
 * })
 * @ORM\Entity()
 */
final class Movie extends AbstractContentMedia
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=5)
     */
    private $duration;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gallery", mappedBy="movie", cascade={"all"})
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     * })
     */
    private $gallery;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Genre", inversedBy="movie", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="movie_genres")
     */
    private $genres;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Cast", inversedBy="movie", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="movie_credits_cast")
     */
    private $casts;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Character", inversedBy="movie", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="movie_credits_character")
     */
    private $characters;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\BroadcastPlatform", mappedBy="movies")
     */
    private $broadcastPlatforms;

    /**
     * Default contructor.
     */
    public function __construct()
    {
        $this->gallery = new \Doctrine\Common\Collections\ArrayCollection();
        $this->genres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->casts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->characters = new \Doctrine\Common\Collections\ArrayCollection();
        $this->broadcastPlatforms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return Movie
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Movie
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Add gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return Movie
     */
    public function addGallery(\AppBundle\Entity\Gallery $gallery)
    {
        $this->gallery[] = $gallery;

        return $this;
    }

    /**
     * Remove gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGallery(\AppBundle\Entity\Gallery $gallery)
    {
        return $this->gallery->removeElement($gallery);
    }

    /**
     * Get gallery
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Add genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return Movie
     */
    public function addGenre(\AppBundle\Entity\Genre $genre)
    {
        $this->genres[] = $genre;

        return $this;
    }

    /**
     * Remove genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGenre(\AppBundle\Entity\Genre $genre)
    {
        return $this->genres->removeElement($genre);
    }

    /**
     * Get genres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Add cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return Movie
     */
    public function addCast(\AppBundle\Entity\Cast $cast)
    {
        $this->casts[] = $cast;

        return $this;
    }

    /**
     * Remove cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCast(\AppBundle\Entity\Cast $cast)
    {
        return $this->casts->removeElement($cast);
    }

    /**
     * Get casts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCasts()
    {
        return $this->casts;
    }

    /**
     * Add character
     *
     * @param \AppBundle\Entity\Character $character
     *
     * @return Movie
     */
    public function addCharacter(\AppBundle\Entity\Character $character)
    {
        $this->characters[] = $character;

        return $this;
    }

    /**
     * Remove character
     *
     * @param \AppBundle\Entity\Character $character
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCharacter(\AppBundle\Entity\Character $character)
    {
        return $this->characters->removeElement($character);
    }

    /**
     * Get characters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Get broadcastPlatforms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBroadcastPlatforms()
    {
        return $this->broadcastPlatforms;
    }

}
