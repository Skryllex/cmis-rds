<?php
/**
 * @file
 * Entity Quality.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableMasterTableEntity;

/**
 * Quality.
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_quality", columns={"code", "title"})
 *   },
 *   indexes={
 *      @ORM\Index(name="quality_index", columns={"code", "title"}),
 * })
 * @ORM\Entity()
 */
class Quality extends AbstractTranslatableMasterTableEntity
{

}
