<?php
/**
 * @file
 * Entity TypeRepeatViewing.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableMasterTableEntity;

/**
 * TypeRepeatViewing.
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_type_repeat_viewing", columns={"code", "title"})
 *   },
 *   indexes={
 *      @ORM\Index(name="type_repeat_viewing_index", columns={"code", "title"}),
 * })
 * @ORM\Entity()
 */
final class TypeRepeatViewing extends AbstractTranslatableMasterTableEntity
{
}