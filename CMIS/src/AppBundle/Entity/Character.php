<?php
/**
 * @file
 * Entity Character.
 */
namespace AppBundle\Entity;

use AppBundle\Entity\Common\AbstractContentMediaCredit;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * Character
 *
 * @ORM\Table(name="characters", indexes={
 *      @ORM\Index(name="CHARACTER_IDX", columns={"name"}),
 * })
 * @ORM\Entity()
 * @DoctrineAssert\UniqueEntity(fields={"name", "surname", "slug"})
 */
final class Character extends AbstractContentMediaCredit
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gallery", mappedBy="character")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     * })
     */
    private $gallery;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Cast", mappedBy="characters")
     */
    private $cast;

    /**
     *
     * @var \AppBundle\Entity\Series
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Series", mappedBy="characters")
     */
    private $series;

    /**
     *
     * @var \AppBundle\Entity\Movie
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Movie", mappedBy="characters")
     */
    private $movie;

    /**
     *
     * @var \AppBundle\Entity\Chapter
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Chapter", mappedBy="characters")
     */
    private $chapter;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->gallery = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return Character
     */
    public function addGallery(\AppBundle\Entity\Gallery $gallery)
    {
        $this->gallery->add($gallery);

        return $this;
    }

    /**
     * Remove gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGallery(\AppBundle\Entity\Gallery $gallery)
    {
        return $this->gallery->removeElement($gallery);
    }

    /**
     * Get gallery
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Get cast
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCast()
    {
        return $this->cast;
    }

    /**
     * Get series
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Get movie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Get chapter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChapter()
    {
        return $this->chapter;
    }
}
