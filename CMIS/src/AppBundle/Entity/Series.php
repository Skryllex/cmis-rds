<?php
/**
 * @file
 * Entity Series.
 */
namespace AppBundle\Entity;

use AppBundle\Entity\Common\AbstractContentMedia;
use Doctrine\ORM\Mapping as ORM;

/**
 * Series
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="SERIES_IDX", columns={"year", "title"}),
 * })
 * @ORM\Entity()
 */
final class Series extends AbstractContentMedia
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="year_end", type="string", length=255, nullable=true)
     */
    private $yearEnd;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gallery", mappedBy="series", cascade={"all"})
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     * })
     */
    private $gallery;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\StateSeries", inversedBy="series", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="series_state_series")
     */
    private $statesSeries;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Season", inversedBy="series", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="series_seasons")
     */
    private $seasons;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Chapter", inversedBy="series", cascade={"all"})
     * @ORM\JoinTable(name="series_chapters")
     */
    private $chapters;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Genre", inversedBy="series", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="series_genres")
     */
    private $genres;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Cast", inversedBy="series", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="series_credits_cast")
     */
    private $casts;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Character", inversedBy="series", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="series_credits_character")
     */
    private $characters;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\BroadcastPlatform", mappedBy="series")
     */
    private $broadcastPlatforms;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->gallery = new \Doctrine\Common\Collections\ArrayCollection();
        $this->seasons = new \Doctrine\Common\Collections\ArrayCollection();
        $this->statesSeries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->genres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->casts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->characters = new \Doctrine\Common\Collections\ArrayCollection();
        $this->chapters = new \Doctrine\Common\Collections\ArrayCollection();
        $this->broadcastPlatforms = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set yearEnd
     *
     * @param string $yearEnd
     *
     * @return Series
     */
    public function setYearEnd($yearEnd)
    {
        $this->yearEnd = $yearEnd;

        return $this;
    }

    /**
     * Get yearEnd
     *
     * @return string
     */
    public function getYearEnd()
    {
        return $this->yearEnd;
    }

    /**
     * Add gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return Series
     */
    public function addGallery(\AppBundle\Entity\Gallery $gallery)
    {
        $this->gallery->add($gallery);

        return $this;
    }

    /**
     * Remove gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGallery(\AppBundle\Entity\Gallery $gallery)
    {
        return $this->gallery->removeElement($gallery);
    }

    /**
     * Get gallery
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Add statesSeries
     *
     * @param \AppBundle\Entity\StateSeries $statesSeries
     *
     * @return Series
     */
    public function addStatesSeries(\AppBundle\Entity\StateSeries $statesSeries)
    {
        $this->statesSeries->add($statesSeries);

        return $this;
    }

    /**
     * Remove statesSeries
     *
     * @param \AppBundle\Entity\StateSeries $statesSeries
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStatesSeries(\AppBundle\Entity\StateSeries $statesSeries)
    {
        return $this->statesSeries->removeElement($statesSeries);
    }

    /**
     * Get statesSeries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatesSeries()
    {
        return $this->statesSeries;
    }

    /**
     * Add season
     *
     * @param \AppBundle\Entity\Season $season
     *
     * @return Series
     */
    public function addSeason(\AppBundle\Entity\Season $season)
    {
        $this->seasons->add($season);

        return $this;
    }

    /**
     * Remove season
     *
     * @param \AppBundle\Entity\Season $season
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSeason(\AppBundle\Entity\Season $season)
    {
        return $this->seasons->removeElement($season);
    }

    /**
     * Get seasons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeasons()
    {
        return $this->seasons;
    }

    /**
     * Add chapter
     *
     * @param \AppBundle\Entity\Chapter $chapter
     *
     * @return Series
     */
    public function addChapter(\AppBundle\Entity\Chapter $chapter)
    {
        $this->chapters->add($chapter);

        return $this;
    }

    /**
     * Remove chapter
     *
     * @param \AppBundle\Entity\Chapter $chapter
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChapter(\AppBundle\Entity\Chapter $chapter)
    {
        return $this->chapters->removeElement($chapter);
    }

    /**
     * Get chapters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChapters()
    {
        return $this->chapters;
    }

    /**
     * Add genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return Series
     */
    public function addGenre(\AppBundle\Entity\Genre $genre)
    {
        $this->genres->add($genre);

        return $this;
    }

    /**
     * Remove genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGenre(\AppBundle\Entity\Genre $genre)
    {
        return $this->genres->removeElement($genre);
    }

    /**
     * Get genres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Add cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return Series
     */
    public function addCast(\AppBundle\Entity\Cast $cast)
    {
        $this->casts->add($cast);

        return $this;
    }

    /**
     * Remove cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCast(\AppBundle\Entity\Cast $cast)
    {
        return $this->casts->removeElement($cast);
    }

    /**
     * Get casts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCasts()
    {
        return $this->casts;
    }

    /**
     * Add character
     *
     * @param \AppBundle\Entity\Character $character
     *
     * @return Series
     */
    public function addCharacter(\AppBundle\Entity\Character $character)
    {
        $this->characters->add($character);

        return $this;
    }

    /**
     * Remove character
     *
     * @param \AppBundle\Entity\Character $character
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCharacter(\AppBundle\Entity\Character $character)
    {
        return $this->characters->removeElement($character);
    }

    /**
     * Get characters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Get broadcastPlatforms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBroadcastPlatforms()
    {
        return $this->broadcastPlatforms;
    }

}
