<?php
/**
 * @file
 * Entity TypeCast.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableMasterTableEntity;

/**
 * TypeCast.
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_type_cast", columns={"code", "title"})
 *   },
 *   indexes={
 *      @ORM\Index(name="type_cast_index", columns={"code", "title"}),
 * })
 * @ORM\Entity()
 */
class TypeCast extends AbstractTranslatableMasterTableEntity
{

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Cast", mappedBy="typesCast")
     */
    private $cast;

    /**
     * Get cast.
     */
    public function getCast()
    {
        return $this->cast;
    }

}
