<?php
/**
 * @file
 * Defining the basic entity with common properties for entities.
 */
namespace AppBundle\Entity\Common;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Basic Entity
 *
 * @ORM\MappedSuperclass(repositoryClass="AppBundle\Entity\Common\Repository\BasicEntityRepository")
 *
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/loggable.md
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/translatable.md
 */
abstract class AbstractContentMediaCredit extends AbstractTranslatableEntity
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="The field is required.")
     */
    protected $name;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="sur_name", type="string", length=255, nullable=true, unique=true)
     */
    protected $surname;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="biography", type="string", length=10000, nullable=true)
     * @Gedmo\Translatable()
     */
    protected $biography;

    /**
     * Set name
     *
     * @param string $name
     * @return \AppBundle\Entity\Common\BasicContentMediaCreditEntity
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return \AppBundle\Entity\Common\BasicContentMediaCreditEntity
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set biography
     *
     * @param string $biography
     * @return \AppBundle\Entity\Common\BasicContentMediaCreditEntity
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;
        return $this;
    }

    /**
     * Get biography
     *
     * @return string $biography.
     */
    public function getBiography()
    {
        return $this->biography;
    }
}