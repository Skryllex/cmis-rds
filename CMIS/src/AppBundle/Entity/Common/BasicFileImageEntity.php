<?php
/**
 * @file
 * Defining the basic entity with common properties for entity files.
 */
namespace AppBundle\Entity\Common;

use AppBundle\Entity\Common\BasicFileEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 */
abstract class BasicFileImageEntity extends BasicFileEntity
{

    /**
     * @Assert\Image(maxSize="2M", mimeTypes={"image/jpg", "image/jpeg", "image/png"})
     */
    protected $file;

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Common\BasicFileEntity::getUploadDir()
     */
    protected function getUploadDir()
    {
        return self::UPLOAD_DIR . '/' . __CLASS__ . '/images';
    }

}