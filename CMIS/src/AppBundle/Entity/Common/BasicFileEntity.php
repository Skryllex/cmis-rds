<?php
/**
 * @file
 * Defining the basic entity with common properties for entity files.
 */
namespace AppBundle\Entity\Common;

use Doctrine\ORM\Mapping as ORM;
use SylrSyksSoftSymfony\CoreBundle\Entity\BaseEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 *
 * @ORM\MappedSuperclass()
 * @ORM\HasLifecycleCallbacks()
 *
 * @link http://gitnacho.github.io/symfony-docs-es/cookbook/doctrine/file_uploads.html
 */
abstract class BasicFileEntity extends BaseEntity
{

    const UPLOAD_ROOT_DIR = __DIR__ . '/../../../../web/';

    const UPLOAD_DIR = "uploads/cmis";

    const INITIAL = 'initial';

    /**
     *
     * @var string
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    protected $path;

    protected $file;

    protected $temp;

    /**
     * Get absolute path.
     *
     * @return NULL|string
     */
    public function getAbsolutePath()
    {
        return (NULL === $this->path) ? NULL : $this->getUploadRootDir() . '/' . $this->path;
    }

    /**
     * Get web path.
     *
     *
     * @return NULL|string
     */
    public function getWebPath()
    {
        return (NULL === $this->path) ? NULL : $this->getUploadDir() . '/' . $this->path;
    }

    /**
     * Get the absolute path of the directory where you should save files uploaded.
     *
     * @return string
     */
    protected function getUploadRootDir()
    {
        return self::UPLOAD_ROOT_DIR . $this->getUploadDir();
    }

    /**
     * Get relative upload dir.
     * Not include root dir.
     *
     * @return string
     */
    protected function getUploadDir()
    {
        return self::UPLOAD_DIR . '/' . __CLASS__ . '/files';
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     * @return \AppBundle\Entity\Common\BasicFileEntity
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = NULL;
        } else {
            $this->path = self::INITIAL;
        }

        return $this;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Generate unique name file.
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (NULL !== $this->getFile()) {
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (NULL === $this->getFile()) {
            return;
        }

        // This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old file
        if (isset($this->temp)) {
            // delete the old fule
            unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp file path
            $this->temp = NULL;
        }
        $this->file = NULL;
    }

    /**
     * Remove file.
     *
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

}