<?php
/**
 * @file
 * Defining the basic entity with common properties for entities.
 */
namespace AppBundle\Entity\Common;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Basic Entity
 *
 * @ORM\MappedSuperclass()
 * @DoctrineAssert\UniqueEntity(fields={"title", "year", "country", "slug"})
 * @Gedmo\Loggable()
 *
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/loggable.md
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/translatable.md
 */
abstract class AbstractContentMedia extends AbstractTranslatableEntity
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\Translatable()
     * @Gedmo\Versioned()
     */
    protected $title;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="synopsis", type="string", length=10000)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\Translatable()
     */
    protected $synopsis;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="year", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="The field is required.")
     */
    protected $year;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=100, unique=true)
     * @Assert\NotBlank(message="The field is required.")
     * @Assert\Country()
     */
    protected $country;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     * @Gedmo\Slug(fields={"year", "title"})
     */
    protected $slug;

    /*
     * ODM Relationships.
     */

    /**
     * Set title
     *
     * @param string $title
     * @return \AppBundle\Entity\Common\BasicContentMediaEntity
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set synopsis
     *
     * @param string $synopsis
     * @return \AppBundle\Entity\Common\BasicContentMediaEntity
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;
        return $this;
    }

    /**
     * Get synopsis
     *
     * @return string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * Set year
     *
     * @param string $year
     * @return \AppBundle\Entity\Common\BasicContentMediaEntity
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return \AppBundle\Entity\Common\BasicContentMediaEntity
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get slug.
     *
     * @return string $slug.
     */
    public function getSlug()
    {
        return $this->slug;
    }
}