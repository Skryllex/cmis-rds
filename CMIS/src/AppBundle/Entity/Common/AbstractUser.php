<?php
/**
 * @file
 * Basic entity user.
 */
namespace AppBundle\Entity\Common;

use AppBundle\Entity\Group;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Sonata\UserBundle\Entity\BaseUser;
use SylrSyksSoftSymfony\CoreBundle\Bundle\BundleInterface;
use SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * Basic User Entity
 *
 * @ORM\MappedSuperclass()
 * @DoctrineAssert\UniqueEntity(fields={"username", "email"})
 */
abstract class AbstractUser extends BaseUser implements BundleInterface, Translatable, ModelInterface
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable()
     */
    protected $biography;

    /**
     *
     * @var string
     *
     * @Gedmo\Slug(fields={"username", "email"})
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     */
    protected $slug;

    /**
     *
     * @var string
     */
    protected static $bundle;

    /**
     * @var string
     *
     * @Gedmo\Locale()
     */
    protected $tranlatableLocale;

    /**
     * Get slug.
     *
     * @return string $slug.
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * {@inheritdoc}
     *
     * @see SylrSyksSoftSymfony\CoreBundle\Bundle\BundleInterface::getBundle()
     */
    public static function getBundle($bundleName = 'AppBundle')
    {
        if (NULL === self::$bundle) {
            $class = new \ReflectionClass(static::class);
            self::$bundle = $bundleName . ':' . $class->getShortName();
        }
        return self::$bundle;
    }

    /**
     * Set locale.
     *
     * @param string $locale
     * @return \AppBundle\Document\Common\BasicUser
     */
    public function setTranslatableLocale($locale)
    {
        $this->tranlatableLocale = $locale;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface::setDeletedAt()
     */
    public function setDeletedAt(\DateTime $deletedAt = NULL)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }


    /**
     *
     * {@inheritDoc}
     *
     * @see \SylrSyksSoftSymfony\CoreBundle\Model\ModelInterface::getDeletedAt()
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add element into collection.
     *
     * @param BasicGroup $entity
     *            Object to add.
     * @param string $oneToManyProperty
     *            Name of one to many property.
     */
    public function __add(Group $entity, $oneToManyProperty)
    {
        $get = 'get' . ucwords($oneToManyProperty);
        if (method_exists($this, $get) && ! $this->$get()->contains($entity)) {
            $add = 'add' . ucwords($oneToManyProperty);
            if (method_exists($this, $add)) {
                return $this->$add($entity);
            }
        }

        return $this;
    }
}