<?php
/**
 * @file
 * Basic Repository.
 */
namespace AppBundle\Entity\Common\Repository;

use Doctrine\ORM\EntityRepository;

class BasicEntityRepository extends EntityRepository
{

    /**
     * Find elements by relationship.
     *
     * @param array $conditions
     *            Conditions.
     * @return unknown
     */
    public function findByRelationship($query)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository($query['bundle'])->createQueryBuilder('rel');
        if (isset($query['relationships']) && ! empty($query['relationships'])) {
            foreach ($query['relationships'] as $field) {
                if (! empty($field['conditions'])) {
                    $qb->join($field['entity'], $field['alias'], $field['condition_type'], $field['conditions']);
                } else {
                    $qb->join('rel.' . $field['field_name'], $field['alias']);
                }
            }
        }

        if (isset($query['conditions']) && ! empty($query['conditions'])) {
            if (! is_array($query['conditions'])) {
                $qb->where($query['conditions']);
            } else {
                foreach ($query['conditions'] as $ctype => $condition) {
                    switch ($ctype) {
                        case 'or':
                            $qb->orWhere($condition);
                            break;

                        case 'and':
                        default:
                            $qb->andWhere($condition);
                            break;
                    }
                }
            }
        }

        if (isset($query['order_by']) && ! empty($query['order_by'])) {
            $qb->orderBy($query['order_by']);
        }

        try {
            $query = $qb->getQuery();
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return FALSE;
        }
    }

}