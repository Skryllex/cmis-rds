<?php
/**
 * @file
 * Entity Season.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableMasterTableEntity;

/**
 * Season.
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_season", columns={"code", "title"})
 *   },
 *   indexes={
 *      @ORM\Index(name="season_index", columns={"code", "title"}),
 * })
 * @ORM\Entity()
 */
final class Season extends AbstractTranslatableMasterTableEntity
{

    /**
     *
     * @var \AppBundle\Entity\Series
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Series", mappedBy="seasons")
     */
    private $series;

    /**
     * Get genres.
     */
    public function getSeries()
    {
        return $this->series;
    }

}
