<?php
/**
 * @file
 * Entity StateSeries.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableMasterTableEntity;

/**
 * StateSeries.
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_state_series", columns={"code", "title"})
 *   },
 *   indexes={
 *      @ORM\Index(name="state_series_index", columns={"code", "title"}),
 * })
 * @ORM\Entity()
 */
final class StateSeries extends AbstractTranslatableMasterTableEntity
{

    /**
     *
     * @var \AppBundle\Entity\Series
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Series", mappedBy="statesSeries")
     */
    private $series;

    /**
     * Get series.
     */
    public function getSeries()
    {
        return $this->series;
    }

}
