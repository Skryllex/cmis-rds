<?php
/**
 * @file
 * Entity BroadcastPlatform.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BroadcastPlatform
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="BROADCAST_PLATFORM_IDX", columns={"name"})
 * })
 * @ORM\Entity()
 * @DoctrineAssert\UniqueEntity(fields={"name", "slug"})
 * @Gedmo\Loggable()
 *
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/loggable.md
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/translatable.md
 */
final class BroadcastPlatform extends AbstractTranslatableEntity
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000)
     * @Gedmo\Translatable()
     */
    private $description;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=255, nullable=true)
     */
    private $slogan;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="start_transmission", type="datetime", nullable=true)
     * @Assert\NotBlank(message="The field is required.")
     * @Assert\DateTime()
     */
    private $startTransmission;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="web_site", type="string", length=255)
     * @Assert\Url(message= "The url '{{ value }}' is not a valid url")
     */
    private $webSite;

    /**
     *
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     */
    private $slug;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\TypeBroadCastPlatform", inversedBy="broadcastPlatform")
     * @ORM\JoinTable(name="broadcast_platform_types")
     */
    private $typesBroadCastPlatform;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Series", inversedBy="broadcastPlatforms")
     * @ORM\JoinTable(name="broadcast_platform_series")
     */
    private $series;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Movie", inversedBy="broadcastPlatforms")
     * @ORM\JoinTable(name="broadcast_platform_movies")
     */
    private $movies;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Documentary", inversedBy="broadcastPlatforms")
     * @ORM\JoinTable(name="broadcast_platform_documentaries")
     */
    private $documentaries;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->typesBroadCastPlatform = new \Doctrine\Common\Collections\ArrayCollection();
        $this->series = new \Doctrine\Common\Collections\ArrayCollection();
        $this->movies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->documentaries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BroadcastPlatform
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return BroadcastPlatform
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return BroadcastPlatform
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Set startTransmission
     *
     * @param \DateTime $startTransmission
     *
     * @return BroadcastPlatform
     */
    public function setStartTransmission($startTransmission)
    {
        $this->startTransmission = $startTransmission;

        return $this;
    }

    /**
     * Get startTransmission
     *
     * @return \DateTime
     */
    public function getStartTransmission()
    {
        return $this->startTransmission;
    }

    /**
     * Set webSite
     *
     * @param string $webSite
     *
     * @return BroadcastPlatform
     */
    public function setWebSite($webSite)
    {
        $this->webSite = $webSite;

        return $this;
    }

    /**
     * Get webSite
     *
     * @return string
     */
    public function getWebSite()
    {
        return $this->webSite;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add typesBroadCastPlatform
     *
     * @param \AppBundle\Entity\TypeBroadCastPlatform $typesBroadCastPlatform
     *
     * @return BroadcastPlatform
     */
    public function addTypesBroadCastPlatform(\AppBundle\Entity\TypeBroadCastPlatform $typesBroadCastPlatform)
    {
        $this->typesBroadCastPlatform[] = $typesBroadCastPlatform;

        return $this;
    }

    /**
     * Remove typesBroadCastPlatform
     *
     * @param \AppBundle\Entity\TypeBroadCastPlatform $typesBroadCastPlatform
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTypesBroadCastPlatform(\AppBundle\Entity\TypeBroadCastPlatform $typesBroadCastPlatform)
    {
        return $this->typesBroadCastPlatform->removeElement($typesBroadCastPlatform);
    }

    /**
     * Get typesBroadCastPlatform
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTypesBroadCastPlatform()
    {
        return $this->typesBroadCastPlatform;
    }

    /**
     * Add series
     *
     * @param \AppBundle\Entity\Series $series
     *
     * @return BroadcastPlatform
     */
    public function addSeries(\AppBundle\Entity\Series $series)
    {
        $this->series[] = $series;

        return $this;
    }

    /**
     * Remove series
     *
     * @param \AppBundle\Entity\Series $series
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSeries(\AppBundle\Entity\Series $series)
    {
        return $this->series->removeElement($series);
    }

    /**
     * Get series
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Add movie
     *
     * @param \AppBundle\Entity\Movie $movie
     *
     * @return BroadcastPlatform
     */
    public function addMovie(\AppBundle\Entity\Movie $movie)
    {
        $this->movies[] = $movie;

        return $this;
    }

    /**
     * Remove movie
     *
     * @param \AppBundle\Entity\Movie $movie
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMovie(\AppBundle\Entity\Movie $movie)
    {
        return $this->movies->removeElement($movie);
    }

    /**
     * Get movies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }

    /**
     * Add documentary
     *
     * @param \AppBundle\Entity\Documentary $documentary
     *
     * @return BroadcastPlatform
     */
    public function addDocumentary(\AppBundle\Entity\Documentary $documentary)
    {
        $this->documentaries[] = $documentary;

        return $this;
    }

    /**
     * Remove documentary
     *
     * @param \AppBundle\Entity\Documentary $documentary
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDocumentary(\AppBundle\Entity\Documentary $documentary)
    {
        return $this->documentaries->removeElement($documentary);
    }

    /**
     * Get documentaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocumentaries()
    {
        return $this->documentaries;
    }
}
