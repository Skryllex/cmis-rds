<?php
/**
 * @file
 * Entity Group.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\UserBundle\Entity\BaseGroup;
use SylrSyksSoftSymfony\CoreBundle\Bundle\BundleInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use SylrSyksSoftSymfony\CoreBundle\Bundle\BundleTrait;

/**
 * Group
 *
 * @ORM\Table(name="groups", indexes={
 *      @ORM\Index(name="GROUP_IDX", columns={"code", "name"})
 * })
 * @DoctrineAssert\UniqueEntity(fields={"code", "name"})
 * @ORM\Entity()
 * @Gedmo\Loggable()
 *
 */
final class Group extends BaseGroup implements BundleInterface
{
    use BundleTrait;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="code", type="string")
     * @Gedmo\Versioned()
     * @Assert\NotBlank(message="The field is required.")
     * @Assert\Length(
     *      min=3,
     *      max=50,
     *      minMessage="The code is too short.",
     *      maxMessage="The code is too long."
     * )
     */
    private $code;

    /**
     * Set code
     *
     * @param string $code
     * @return \AppBundle\Entity\Group
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

}
