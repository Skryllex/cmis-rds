<?php
/**
 * @file
 * Entity Chapter.
 */
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Chapter
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="CHAPTER_IDX", columns={"title"}),
 * })
 * @ORM\Entity(repositoryClass="Gedmo\Sortable\Entity\Repository\SortableRepository")
 * @DoctrineAssert\UniqueEntity(fields={"slug"})
 * @Gedmo\Loggable()
 *
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/loggable.md
 * @link https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/translatable.md
 *
 */
final class Chapter extends AbstractTranslatableEntity
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\SortableGroup()
     * @Gedmo\Translatable()
     * @Gedmo\Versioned()
     */
    private $title;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="synopsis", type="string", length=765)
     * @Assert\NotBlank(message="The field is required.")
     * @Gedmo\Translatable()
     */
    private $synopsis;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=5)
     */
    private $duration;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_issue", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $dateOfIssue;

    /**
     *
     * @var string
     *
     * @Gedmo\SortablePosition()
     * @ORM\Column(name="position", type="integer")
     */
    private $weight;

    /**
     *
     * @var string
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     */
    private $slug;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gallery", mappedBy="chapter", cascade={"all"})
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     * })
     */
    private $gallery;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Series", mappedBy="chapters")
     */
    private $series;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Cast", inversedBy="chapter", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="chapter_credits_cast")
     */
    private $casts;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Character", inversedBy="chapter", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="chapter_credits_character")
     */
    private $characters;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->gallery = new \Doctrine\Common\Collections\ArrayCollection();
        $this->casts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->characters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Chapter
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set synopsis
     *
     * @param string $synopsis
     *
     * @return Chapter
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * Get synopsis
     *
     * @return string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return Chapter
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set dateOfIssue
     *
     * @param \DateTime $dateOfIssue
     *
     * @return Chapter
     */
    public function setDateOfIssue($dateOfIssue)
    {
        $this->dateOfIssue = $dateOfIssue;

        return $this;
    }

    /**
     * Get dateOfIssue
     *
     * @return \DateTime
     */
    public function getDateOfIssue()
    {
        return $this->dateOfIssue;
    }

    /**
     * Set weight
     *
     * @param int $weight
     *
     * @return Chapter
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Get slug.
     *
     * @return string $slug.
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return Chapter
     */
    public function addGallery(\AppBundle\Entity\Gallery $gallery)
    {
        $this->gallery->add($gallery);

        return $this;
    }

    /**
     * Remove gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGallery(\AppBundle\Entity\Gallery $gallery)
    {
        return $this->gallery->removeElement($gallery);
    }

    /**
     * Get gallery
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Get series
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Add cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return Chapter
     */
    public function addCast(\AppBundle\Entity\Cast $cast)
    {
        $this->casts->add($cast);

        return $this;
    }

    /**
     * Remove cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCast(\AppBundle\Entity\Cast $cast)
    {
        return $this->casts->removeElement($cast);
    }

    /**
     * Get casts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCasts()
    {
        return $this->casts;
    }

    /**
     * Add character
     *
     * @param \AppBundle\Entity\Character $character
     *
     * @return Chapter
     */
    public function addCharacter(\AppBundle\Entity\Character $character)
    {
        $this->characters->add($character);

        return $this;
    }

    /**
     * Remove character
     *
     * @param \AppBundle\Entity\Character $character
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCharacter(\AppBundle\Entity\Character $character)
    {
        return $this->characters->removeElement($character);
    }

    /**
     * Get characters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacters()
    {
        return $this->characters;
    }

}
