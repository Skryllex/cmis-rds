<?php
/**
 * @file
 * Entity utils.
 */
namespace AppBundle\Entity\Utils;

use SylrSyksSoftSymfony\CoreBundle\Util\Singleton;

final class EntityUtils
{
    use Singleton;

    public function get(\Doctrine\ORM\EntityManager $manager, $class, array $conditions)
    {}

    /**
     *
     * @param \Doctrine\ORM\EntityManager $manager
     *            Entity manager.
     * @param array|\AppBundle\Entity\Common\BasicEntity $entity
     *            Entity to add.
     * @param array $conditions
     *            Conditions.
     * @param \AppBundle\Entity\Common\BasicEntity $e
     *            Entity.
     * @param \AppBundle\Entity\Builders\Entity\BasicBuilder $builder
     *            Builder.
     */
    public function add(\Doctrine\ORM\EntityManager $manager, $entity, $e, \AppBundle\Entity\Builders\Entity\BasicBuilder $builder, ...$parameters_builder)
    {
        if (! isset($e)) {
            $e = call_user_func_array(array(
                $builder,
                'build'
            ), $parameters_builder);
            if (isset($entity['translation']) && ! empty($entity['translation'])) {
                $e = $builder->addTranslation($e, $manager, $entity['translation']);
            }
        }

        return $e;
    }
}