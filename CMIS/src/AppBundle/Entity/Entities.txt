The application consists of the following entities:


Aplication Entities:

PK - Primary Key
NN - Not Null.
AI - Auto Incremental.
DFN - Default Null
UQ - Unique

Broadcast Platform:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
name var_char(255)||NN|)
description var_char(510)||NN|)
start_transmission (date_time||NN|)
web_site var_char(255)||NN|)
slug var_char(128)||NN|UQ)
Index: broadcast_platform_idx(name)

Broadcast Platform Documentaries:
broadcast_platform_id (bigint(20)|PK|NN)
documentary_id (bigint(20)|PK|NN)
Foreign keys: broadcast_platform, documentary

Broadcast Platform Movies:
broadcast_platform_id (bigint(20)|PK|NN)
movie_id (bigint(20)|PK|NN)
Foreign keys: broadcast_platform, movie

Broadcast Platform Series:
broadcast_platform_id (bigint(20)|PK|NN)
series_id (bigint(20)|PK|NN)
Foreign keys: broadcast_platform, series

Cast:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
name var_char(255)||NN|)
sur_name var_char(255)||NN|)
birth_date (date_time||NN|)
biography var_char(510)||NN|)
slug var_char(128)||NN|UQ)
series (bigint(20)|FK|DFN|)
movie (bigint(20)|FK|DFN|)
chapter (bigint(20)|FK|DFN|)
documentary (bigint(20)|FK|DFN|)
Foreign keys: series, movie, chapter, documentary
Index: cast_idx(name, sur_name)

Chapter:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|)
synopsis var_char(765)||NN|)
duration (time||NN|)
date_of_issue (date_time||NN|)
slug var_char(128)||NN|UQ)
season (bigint(20)|FK|NN|)
Foreign keys: season
Index: chapter_idx(title)

Character:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
name var_char(255)||NN|)
sur_name var_char(255)||NN|)
slug var_char(128)||NN|UQ)
series (bigint(20)|FK|DFN|)
movie (bigint(20)|FK|DFN|)
chapter (bigint(20)|FK|DFN|)
documentary (bigint(20)|FK|DFN|)
cast (bigint(20)|FK|DFN|)
Foreign keys: series, movie, chapter, documentary, cast
Index: character_idx(name, sur_name)

Documentary:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|)
synopsis var_char(765)||NN|)
year var_char(255)||NN|)
country var_char(100)||NN|)
slug var_char(128)||NN|UQ)
Index: documentary_idx (title)

Genre:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|UQ)
description var_char(510)||DFN|)
slug var_char(128)||NN|UQ)
series (bigint(20)|FK|DFN|)
movie (bigint(20)|FK|DFN|)
chapter (bigint(20)|FK|DFN|)
documentary (bigint(20)|FK|DFN|)
Foreign keys: series, movie, documentary
Index: genre_idx(code, title)

Image:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
name var_char(255)||NN|)
title var_char(255)||DFN|)
alternative_text var_char(255)||DFN|)
path var_char(255)||DFN|)
slug var_char(128)||NN|UQ)
series (bigint(20)|FK|DFN|)
movie (bigint(20)|FK|DFN|)
cast (bigint(20)|FK|DFN|)
documentary (bigint(20)|FK|DFN|)
broadcastPlatform (bigint(20)|FK|DFN|)
user (bigint(20)|FK|DFN|)
character (bigint(20)|FK|DFN|)
Foreign keys: series, movie, documentary, cast, character, broadcast_platform, user
Index: image_idx(name)

Movie:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|)
synopsis var_char(765)||NN|)
year var_char(255)||NN|)
country var_char(100)||NN|)
slug var_char(128)||NN|UQ)
Index: movie_idx (title)

Quality:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|UQ)
description var_char(510)||DFN|)
slug var_char(128)||NN|UQ)
Index: quality_idx(code, title)

Season:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|UQ)
description var_char(510)||DFN|)
slug var_char(128)||NN|UQ)
series (bigint(20)|FK|DFN|)
Foreign keys: series
Index: season_idx(code, title)

Series:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|)
synopsis var_char(765)||NN|)
year var_char(255)||NN|)
country var_char(100)||NN|)
slug var_char(128)||NN|UQ)
Index: series_idx (title)

State series:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|UQ)
description var_char(510)||DFN|)
slug var_char(128)||NN|UQ)
series (bigint(20)|FK|DFN|)
Foreign keys: series
Index: state_series_idx(code, title)

Type cast:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|UQ)
description var_char(510)||DFN|)
slug var_char(128)||NN|UQ)
cast (bigint(20)|FK|DFN|)
Foreign keys: cast
Index: type_cast_idx(code, title)

Type repeat viewing:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
title var_char(255)||NN|UQ)
description var_char(510)||DFN|)
slug var_char(128)||NN|UQ)
Index: type_repeat_viewing_idx(code, title)

User:
id (bigint(20)|PK|NN|AI)
start_date (date_time||NN|)
modification_date (date_time||NN|)
end_date (date_time||DFN|)
nick_name var_char(255)||NN|UQ)
name var_char(255)||NN|)
sur_name var_char(255)||NN|)
email var_cahr(255)||NN|UQ)
address var_char(510)||NN|)
birth_date (datetime||NN|)
state (tinyint(1)||NN|1)
password var_char(255)||NN|)
salt var_char(255)||NN|)
slug var_char(128)||NN|UQ)
Index: user_idx(nick_name, name, surname)
