<?php

/**
 * This file is part of the <name> project.
 *
 * (c) <yourname> <youremail>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\MediaBundle\Entity\BaseGallery as BaseGallery;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * Gallery
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="GALLERY_IDX", columns={"name"}),
 * })
 * @ORM\Entity()
 * @DoctrineAssert\UniqueEntity(fields={"name"})
 */
final class Gallery extends BaseGallery
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\Chapter
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Chapter", inversedBy="gallery")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="chapter", referencedColumnName="id")
     * })
     */
    private $chapter;

    /**
     * @var \AppBundle\Entity\Character
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Character", inversedBy="gallery")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="character", referencedColumnName="id")
     * })
     */
    private $character;

    /**
     * @var \AppBundle\Entity\Documentary
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Documentary", inversedBy="gallery")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="documentary", referencedColumnName="id")
     * })
     */
    private $documentary;

    /**
     * @var \AppBundle\Entity\Movie
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Movie", inversedBy="gallery")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="movie", referencedColumnName="id")
     * })
     */
    private $movie;

    /**
     * @var \AppBundle\Entity\Series
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Series", inversedBy="gallery")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="series", referencedColumnName="id")
     * })
     */
    private $series;


    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chapter
     *
     * @param \AppBundle\Entity\Chapter $chapter
     *
     * @return Gallery
     */
    public function setChapter(\AppBundle\Entity\Chapter $chapter = null)
    {
        $this->chapter = $chapter;

        return $this;
    }

    /**
     * Get chapter
     *
     * @return \AppBundle\Entity\Chapter
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * Get character
     *
     * @return \AppBundle\Entity\Character
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * Set documentary
     *
     * @param \AppBundle\Entity\Documentary $documentary
     *
     * @return Gallery
     */
    public function setDocumentary(\AppBundle\Entity\Documentary $documentary = null)
    {
        $this->documentary = $documentary;

        return $this;
    }

    /**
     * Get documentary
     *
     * @return \AppBundle\Entity\Documentary
     */
    public function getDocumentary()
    {
        return $this->documentary;
    }

    /**
     * Set movie
     *
     * @param \AppBundle\Entity\Movie $movie
     *
     * @return Gallery
     */
    public function setMovie(\AppBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return \AppBundle\Entity\Movie
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Set series
     *
     * @param \AppBundle\Entity\Series $series
     *
     * @return Gallery
     */
    public function setSeries(\AppBundle\Entity\Series $series = null)
    {
        $this->series = $series;

        return $this;
    }

    /**
     * Get series
     *
     * @return \AppBundle\Entity\Series
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Add galleryHasMedia
     *
     * @param \AppBundle\Entity\GalleryHasMedia $galleryHasMedia
     *
     * @return Gallery
     */
    public function addGalleryHasMedia(\AppBundle\Entity\GalleryHasMedia $galleryHasMedia)
    {
        $this->galleryHasMedias[] = $galleryHasMedia;

        return $this;
    }

    /**
     * Remove galleryHasMedia
     *
     * @param \AppBundle\Entity\GalleryHasMedia $galleryHasMedia
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGalleryHasMedia(\AppBundle\Entity\GalleryHasMedia $galleryHasMedia)
    {
        return $this->galleryHasMedias->removeElement($galleryHasMedia);
    }
}
