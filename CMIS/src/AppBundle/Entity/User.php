<?php
/**
 * @file
 * Entity User.
 */
namespace AppBundle\Entity;

use AppBundle\Entity\Common\AbstractUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="users", indexes={
 *      @ORM\Index(name="USER_IDX", columns={"username", "email", "firstname"})
 * })
 * @ORM\Entity()
 */
final class User extends AbstractUser
{

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="phone_number", nullable=true)
     */
     //@Type("libphonenumber\PhoneNumber")
    protected $phoneNumber;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=true)
     * @Assert\Length(
     *      max=100,
     *      maxMessage="The name is too long.",
     *      groups={"Profile"}
     * )
     */
    private $address;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=10, nullable=true)
     * @Assert\Length(
     *      max=10,
     *      maxMessage="The name is too long.",
     *      groups={"Profile"}
     * )
     */
    private $postcode;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50, nullable=true)
     * @Assert\Length(
     *      max=50,
     *      maxMessage="The name is too long.",
     *      groups={"Profile"}
     * )
     */
    private $city;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=50, nullable=true)
     * @Assert\Length(
     *      max=50,
     *      maxMessage="The name is too long.",
     *      groups={"Profile"}
     * )
     */
    private $country;

    /*
     * Relationships
     */

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Media", mappedBy="user", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="images", referencedColumnName="id")
     * })
     */
    private $images;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->images = new ArrayCollection();
    }



    /**
     * Set phoneNumber
     *
     * @param phone_number $phoneNumber
     *
     * @return User
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return phone_number
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return User
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Media $image
     *
     * @return User
     */
    public function addImage(\AppBundle\Entity\Media $image)
    {
        $this->images->add($image);

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Media $image
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImage(\AppBundle\Entity\Media $image)
    {
        return $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }
}
