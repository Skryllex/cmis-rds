<?php
/**
 * @file
 * Entity Cast.
 */
namespace AppBundle\Entity;

use AppBundle\Entity\Common\AbstractContentMediaCredit;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cast
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="CAST_IDX", columns={"name", "sur_name"}),
 * })
 * @ORM\Entity()
 * @DoctrineAssert\UniqueEntity(fields={"name", "surname", "birthDate", "slug"})
 */
final class Cast extends AbstractContentMediaCredit
{

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $birthDate;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     * @Gedmo\Slug(fields={"name", "surname"})
     */
    private $slug;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\TypeCast", inversedBy="cast")
     * @ORM\JoinTable(name="cast_type_casts")
     */
    private $typesCast;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Character", inversedBy="cast", cascade={"all"})
     * @ORM\JoinTable(name="cast_characters")
     */
    private $characters;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Media", mappedBy="cast")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="images", referencedColumnName="id")
     * })
     */
    private $images;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Series", mappedBy="casts", cascade={"all"})
     */
    private $series;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Movie", mappedBy="casts")
     */
    private $movie;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Chapter", mappedBy="casts")
     */
    private $chapter;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Documentary", mappedBy="casts", )
     */
    private $documentary;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return Cast
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get typesCast
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTypesCast()
    {
        return $this->typesCast;
    }

    /**
     * Get characters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Media $image
     *
     * @return Cast
     */
    public function addImage(\AppBundle\Entity\Media $image)
    {
        $this->images->add($image);

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Media $image
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImage(\AppBundle\Entity\Media $image)
    {
        return $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Get series
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Get movie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Get chapter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * Get documentary
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocumentary()
    {
        return $this->documentary;
    }
}
