<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Entity\Builders\Common;

interface IBasicEntityBuilder
{


    /**
     * Get an object \AppBundle\Entity\Common\BasicEntity.
     */
    public function get();

    /**
     * Persist object \AppBundle\Entity\Common\BasicEntity.
     */
    public function persist();
}