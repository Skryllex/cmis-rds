<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Entity\Builders\Common\BroadCastPlatform;

use AppBundle\Entity\Builders\Common\IBasicEntityBuilder;

interface IBroadcastPlatformBuilder extends IBasicEntityBuilder
{


    /**
     * Set an object \AppBundle\Entity\BroadCastPlatform.
     */
    public function set($name, $description, $website, $slogan = '', $startTransmission = NULL);
}