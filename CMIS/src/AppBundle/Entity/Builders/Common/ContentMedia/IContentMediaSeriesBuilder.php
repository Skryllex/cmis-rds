<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Entity\Builders\Common\ContentMedia;

use AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder;

interface IContentMediaSeriesBuilder extends IContentMediaBuilder
{


    /**
     * Add states.
     *
     * @param array $states
     */
    public function addStateSeries($states);

    /**
     * Add seasons.
     *
     * @param array $seasons
     */
    public function addSeasons($seasons);

    /**
     * Add chapters.
     *
     * @param array $chapters
     */
    public function addChapters($chapters);
}