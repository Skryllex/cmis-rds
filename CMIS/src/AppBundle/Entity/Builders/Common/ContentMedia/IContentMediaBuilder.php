<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Entity\Builders\Common\ContentMedia;

use AppBundle\Entity\Builders\Common\IBasicEntityBuilder;

interface IContentMediaBuilder extends IBasicEntityBuilder
{


    /**
     * Set an object \AppBundle\Entity\Common\BasicContentMediaEntity.
     */
    public function set($title, $synopsis, $year, $country, $yearEnd = NULL, $duration = NULL);

    /**
     * Add genres.
     *
     * @param array $genres
     */
    public function addGenres($genres);

    /**
     * Add casts.
     *
     * @param array $casts
     */
    public function addCasting($casts);

    /**
     * Add characters
     *
     * @param array $characters
     */
    public function addCharacters($characters);
}