<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Entity\Builders\Common\ContentMedia;

use AppBundle\Entity\Builders\Common\IBasicEntityBuilder;

interface IContentMediaChapterBuilder extends IBasicEntityBuilder
{


    /**
     * Set an object \AppBundle\Entity\Chapter.
     */
    public function set($title, $synopsis, $duration, $weight, $dateOfIssue = NULL);

    /**
     * Add casts.
     *
     * @param array $casts
     */
    public function addCasting($casts);

    /**
     * Add characters
     *
     * @param array $characters
     */
    public function addCharacters($characters);
}