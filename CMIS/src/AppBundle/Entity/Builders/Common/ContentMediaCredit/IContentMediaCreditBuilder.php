<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Entity\Builders\Common\ContentMediaCredit;

use AppBundle\Entity\Builders\Common\IBasicEntityBuilder;

interface IContentMediaCreditBuilder extends IBasicEntityBuilder
{


    /**
     * Set an object \AppBundle\Entity\Common\BasicContentMediaCreditEntity.
     */
    public function set($name, $surname, $biography, $birthDate = NULL);

    /**
     * Add typeCast.
     *
     * @param array $typesCast
     */
    public function addTypes($typesCast);

    /**
     * Add characters.
     *
     * @param array $characters
     */
    public function addCharacters($characters);
}