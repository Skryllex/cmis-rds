<?php
/**
 * @file
 * Defining builder.
 */
namespace AppBundle\Entity\Builders\Common\MasterTable;

use AppBundle\Entity\Builders\Common\IBasicEntityBuilder;

interface IMasterTableBuilder extends IBasicEntityBuilder
{


    /**
     * Set an object \AppBundle\Entity\Common\BasicMasterTableEntity.
     */
    public function set($code, $title, $description = '');
}