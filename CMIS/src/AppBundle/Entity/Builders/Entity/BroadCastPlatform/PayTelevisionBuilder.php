<?php
/**
 * @file
 * Builder for PayTelevision type.
 */
namespace AppBundle\Entity\Builders\Entity\BroadCastPlatform;

use AppBundle\Entity\Builders\Common\BroadCastPlatform\IBroadcastPlatformBuilder;
use AppBundle\Entity\Builders\Entity\EntityBuilder;

final class PayTelevisionBuilder extends EntityBuilder implements IBroadcastPlatformBuilder
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\BroadCastPlatform\IBroadcastPlatformBuilder::set()
     */
    public function set($name, $description, $website, $slogan = '', $startTransmission = NULL)
    {
        $this->entity->setName($name)
            ->setDescription($description)
            ->setWebSite($website)
            ->setSlogan($slogan)
            ->setStartTransmission($startTransmission);

        $typeBroadCastPlatform = $this->manager->getRepository(\AppBundle\Entity\TypeBroadCastPlatform::getBundle())->findOneBy(array(
            'code' => \AppBundle\Entity\Builders\Entity\MasterTable\TypeBroadCastPlatformBuilder::CODE_PAY_TELEVISION
        ));
        $this->broadCastPlatform->setTypesBroadCastPlatform($typeBroadCastPlatform);
    }

}