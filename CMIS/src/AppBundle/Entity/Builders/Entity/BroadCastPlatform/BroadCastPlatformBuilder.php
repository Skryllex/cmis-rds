<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\BroadCastPlatform;

use AppBundle\Entity\Builders\Entity\BasicBuilder;

final class BroadCastPlatformBuilder extends BasicBuilder
{

    /**
     * Build object BroadCastPlatform.
     *
     * @param string $name
     *            Name.
     * @param string $description
     *            Description.
     * @param string $website
     *            URL Website.
     * @param string $slogan
     *            Slogan.
     * @param \DateTime $startTransmission
     *            Date of strart transmission.
     */
    public function build($name, $description, $website, $slogan = '', $startTransmission = NULL)
    {
        $this->builder->set($name, $description, $website, $slogan, $startTransmission);
        return $this->builder->get();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Entity\BasicBuilder::getTranlatableProperties()
     */
    public function getTranlatableProperties()
    {
        return array(
            'description'
        );
    }

}