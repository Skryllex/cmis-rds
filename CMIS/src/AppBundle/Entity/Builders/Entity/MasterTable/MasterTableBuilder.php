<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\MasterTable;

use AppBundle\Entity\Builders\Entity\BasicBuilder;

final class MasterTableBuilder extends BasicBuilder
{

    /**
     * Build object \AppBundle\Entity\Common\BasicMasterTableEntity.
     *
     * @param string $code
     *            Code.
     * @param string $title
     *            Title.
     * @param string $description
     *            Description.
     */
    public function build($code, $title, $description = '')
    {
        $this->builder->set($code, $title, $description);
        return $this->builder->get();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Entity\BasicBuilder::getTranlatableProperties()
     */
    public function getTranlatableProperties()
    {
        return array(
            'title',
            'description'
        );
    }

}