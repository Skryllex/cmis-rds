<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\MasterTable;

use AppBundle\Entity\Builders\Common\MasterTable\IMasterTableBuilder;
use AppBundle\Entity\Builders\Entity\EntityBuilder;

class TypeCastBuilder extends EntityBuilder implements IMasterTableBuilder
{
    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\MasterTable\IMasterTableBuilder::set()
     */
    public function set($code, $title, $description = '')
    {
        $this->entity->setCode($code)
            ->setTitle($title)
            ->setDescription($description);
    }
}