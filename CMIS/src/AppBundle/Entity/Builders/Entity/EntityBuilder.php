<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity;

use AppBundle\Entity\Builders\Common\IBasicEntityBuilder;

abstract class EntityBuilder implements IBasicEntityBuilder
{

    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $manager;

    /**
     *
     * @var \AppBundle\Entity\Common\BasicEntity
     */
    protected $entity;

    /**
     * Default constructor.
     *
     * @param \Doctrine\ORM\EntityManager $manager
     *            Object manager.
     */
    public function __construct(\Doctrine\ORM\EntityManager $manager, $className)
    {
        $this->manager = $manager;
        $this->entity = new $className();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\IBasicEntityBuilder::get()
     */
    public function get()
    {
        return $this->entity;
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\IBasicEntityBuilder::persist()
     */
    public function persist()
    {
        $success = FALSE;
        $this->manager->getConnection()->beginTransaction();

        try {
            $this->manager->persist($this->entity);
            $this->manager->getConnection()->commit();
            $this->manager->flush();
            $success = TRUE;
        } catch (Exception $e) {
            $success = FALSE;
            $this->manager->getConnection()->rollBack();
        }

        return $success;
    }
}