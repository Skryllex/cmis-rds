<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity;

use SylrSyksSoftSymfony\CoreBundle\Translator\TranslatorEntityAdapter;

abstract class BasicBuilder
{

    /**
     *
     * @var \AppBundle\Entity\Builders\Common\IBasicEntityBuilder
     */
    protected $builder;

    /**
     * Default constructor.
     *
     * @param \AppBundle\Entity\Builders\Common\BroadCastPlatform\IBroadcastPlatformBuilder $builder
     *            Builder.
     */
    public function __construct(\AppBundle\Entity\Builders\Common\IBasicEntityBuilder $builder)
    {
        $this->builder = $builder;
    }

    /**
     *
     * @return \AppBundle\Entity\Builders\Common\IBasicEntityBuilder
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * Get an array with the translatable properties.
     */
    public abstract function getTranlatableProperties();

    /**
     * Add translatio to entity.
     *
     * @param \AppBundle\Entity\Common\BasicEntity $entity
     * @param array $translation
     * @return \AppBundle\Entity\Common\BasicEntity
     */
    public function addTranslation(\AppBundle\Entity\Common\BasicEntity $entity, \Doctrine\ORM\EntityManager $manager, $translation)
    {
        if (! empty($translation)) {
            $translator = new TranslatorEntityAdapter($entity);
            $translator->translate($manager, $translation, $this->getTranlatableProperties());
            $entity = $translator->getEntity();
        }
        return $entity;
    }
}