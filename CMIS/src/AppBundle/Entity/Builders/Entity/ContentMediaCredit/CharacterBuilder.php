<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMediaCredit;

use AppBundle\Entity\Builders\Common\ContentMediaCredit\IContentMediaCreditBuilder;
use AppBundle\Entity\Builders\Entity\EntityBuilder;

class CharacterBuilder extends EntityBuilder implements IContentMediaCreditBuilder
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMediaCredit\IContentMediaCreditBuilder::set()
     */
    public function set($name, $surname, $biography, $birthDate = NULL)
    {
        $this->entity->setName($name)
            ->setSurname($surname)
            ->setBiography($biography);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMediaCredit\IContentMediaCreditBuilder::addTypes()
     */
    public function addTypes($typesCast)
    {
        return FALSE;
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMediaCredit\IContentMediaCreditBuilder::addCharacters()
     */
    public function addCharacters($characters)
    {
        return FALSE;
    }

}