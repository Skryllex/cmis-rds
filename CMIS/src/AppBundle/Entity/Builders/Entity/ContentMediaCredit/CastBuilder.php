<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMediaCredit;

use AppBundle\Entity\Builders\Common\ContentMediaCredit\IContentMediaCreditBuilder;
use AppBundle\Entity\Builders\Entity\EntityBuilder;

class CastBuilder extends EntityBuilder implements IContentMediaCreditBuilder
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMediaCredit\IContentMediaCreditBuilder::set()
     */
    public function set($name, $surname, $biography, $birthDate = NULL)
    {
        $this->entity->setName($name)
            ->setSurname($surname)
            ->setBiography($biography);

        if (isset($birthDate)) {
            $this->entity->setBirthDate($birthDate);
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMediaCredit\IContentMediaCreditBuilder::addTypes()
     */
    public function addTypes($typesCast)
    {
        if (! empty($typesCast)) {
            foreach ($typesCast as $type) {
                $conditions = array(
                    'code' => $type['code']
                );
                $t = $this->manager->getRepository(\AppBundle\Entity\TypeCast::getBundle())->findOneBy($conditions);

                if (! isset($t)) {
                    $typeCastBuilder = new \AppBundle\Entity\Builders\Entity\MasterTable\TypeCastBuilder($this->manager, \AppBundle\Entity\TypeCast::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\MasterTable\MasterTableBuilder($typeCastBuilder);

                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                    $t = $entityUtils->add($this->manager, $type, $t, $builder, $type['code'], $type['title'], $type['description']);
                }
                $this->entity->__add($t, 'typesCast');
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMediaCredit\IContentMediaCreditBuilder::addCharacters()
     */
    public function addCharacters($characters)
    {
        if (! empty($characters)) {
            foreach ($characters as $character) {
                if (is_object($character)) {
                    $character = \AppBundle\Util\Utils::getInstance()->convertToArray($character);
                }
                $conditions = array(
                    'name' => $character['name']
                );

                if (isset($character['surname'])) {
                    $conditions['surname'] = $character['surname'];
                }

                $c = $this->manager->getRepository(\AppBundle\Entity\Character::getBundle())->findOneBy($conditions);

                if (! isset($c)) {
                    $creditBuilder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\CharacterBuilder($this->manager, \AppBundle\Entity\Character::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\ContentMediaCreditBuilder($creditBuilder);

                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                    $c = $entityUtils->add($this->manager, $character, $c, $builder, $character['name'], $character['surname'], $character['biography']);
                }

                $this->entity->__add($c, 'characters');
            }
        }
    }

}