<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMediaCredit;

use AppBundle\Entity\Builders\Entity\BasicBuilder;

final class ContentMediaCreditBuilder extends BasicBuilder
{

    /**
     * Build object Cast.
     *
     * @param string $name
     *            Name.
     * @param string $surname
     *            Surname.
     * @param string $biography
     *            Biography.
     * @param \DateTime $birthDate
     *            Birth date.
     */
    public function build($name, $surname, $biography, $birthDate = NULL, $typesCast = array(), $characters = array())
    {
        $this->builder->set($name, $surname, $biography, $birthDate);

        if (! empty($typesCast)) {
            $this->builder->addTypes($typesCast);
        }

        if (! empty($characters)) {
            $this->builder->addCharacters($characters);
        }

        return $this->builder->get();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Entity\BasicBuilder::getTranlatableProperties()
     */
    public function getTranlatableProperties()
    {
        return array(
            'biography'
        );
    }

}