<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMedia;

final class ContentMediaSeriesBuilder extends ContentMediaBuilder
{

    /**
     * Content media builder.
     *
     * @param string $title
     *            Title.
     * @param unknown $synopsis
     *            Synopsis.
     * @param string $year
     *            Year.
     * @param string $country
     *            Country.
     * @param array $genres
     *            Genres.
     * @param array $casting
     *            Casting.
     * @param array $characters
     *            Characters.
     * @param string $yearEnd
     *            End year.
     * @param array $chapters
     *            Chapters.
     */
    public function build($title, $synopsis, $year, $country, $genres = array(), $casting = array(), $characters = array(), $yearEnd = NULL, $duration = NULL, $seasons = array(), $states = array(), $chapters = array())
    {
        parent::build($title, $synopsis, $year, $country, $genres, $casting, $characters, $yearEnd, $duration);

        if (! empty($seasons)) {
            $this->builder->addSeasons($seasons);
        }

        if (! empty($states)) {
            $this->builder->addStateSeries($states);
        }

        if (! empty($chapters)) {
            $this->builder->addChapters($chapters);
        }

        return $this->builder->get();
    }

}