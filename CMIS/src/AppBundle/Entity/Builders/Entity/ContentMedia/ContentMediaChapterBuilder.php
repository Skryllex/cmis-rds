<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMedia;

use AppBundle\Entity\Builders\Entity\BasicBuilder;

final class ContentMediaChapterBuilder extends BasicBuilder
{

    /**
     * Build object ContentMedia.
     *
     * @param string $name
     *            Name.
     * @param string $surname
     *            Surname.
     * @param string $biography
     *            Biography.
     * @param \DateTime $birthDate
     *            Birth date.
     */
    public function build($title, $synopsis, $duration, $weight, $dateOfIssue = NULL, $casting = array(), $characters = array())
    {
        $this->builder->set($title, $synopsis, $duration, $weight, $dateOfIssue);

        if (! empty($casting)) {
            $this->builder->addCasting($casting);
        }

        if (! empty($characters)) {
            $this->builder->addCharacters($characters);
        }

        return $this->builder->get();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Entity\BasicBuilder::getTranlatableProperties()
     */
    public function getTranlatableProperties()
    {
        return array(
            'title',
            'synopsis'
        );
    }

}