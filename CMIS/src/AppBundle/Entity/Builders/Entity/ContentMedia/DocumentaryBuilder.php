<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMedia;

use AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder;
use AppBundle\Entity\Builders\Entity\EntityBuilder;

class DocumentaryBuilder extends EntityBuilder implements IContentMediaBuilder
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder::set()
     */
    public function set($title, $synopsis, $year, $country, $yearEnd = NULL, $duration = NULL)
    {
        $this->entity->setTitle($title)
            ->setSynopsis($synopsis)
            ->setYear($year)
            ->setCountry($country)
            ->setDuration($duration);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder::addCasting()
     */
    public function addCasting($casts)
    {
        if (! empty($casts)) {
            foreach ($casts as $cast) {
                if (is_object($cast)) {
                    $cast = \AppBundle\Util\Utils::getInstance()->convertToArray($cast);
                }
                $conditions = array(
                    'name' => $cast['name'],
                    'surname' => $cast['surname']
                );

                $c = $this->manager->getRepository(\AppBundle\Entity\Cast::getBundle())->findOneBy($conditions);

                if (! isset($c)) {
                    $creditBuilder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\CastBuilder($this->manager, \AppBundle\Entity\Cast::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\ContentMediaCreditBuilder($creditBuilder);
                    $birthDate = (isset($cast['birth_date'])) ? \AppBundle\Util\Utils::getInstance()->createDate($cast['birth_date']) : NULL;

                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();

                    if (isset($cast['characters'])) {
                        $c = $entityUtils->add($this->manager, $cast, $c, $builder, $cast['name'], $cast['surname'], $cast['biography'], $birthDate, $cast['types_cast'], $cast['characters']);
                    } else {
                        $c = $entityUtils->add($this->manager, $cast, $c, $builder, $cast['name'], $cast['surname'], $cast['biography'], $birthDate, $cast['types_cast']);
                    }
                }

                $this->entity->__add($c, 'casts');
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder::addCharacters()
     */
    public function addCharacters($characters)
    {
        return FALSE;
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder::addGenres()
     */
    public function addGenres($genres)
    {
        if (! empty($genres)) {
            foreach ($genres as $genre) {
                $conditions = array(
                    'code' => $genre['code']
                );
                $g = $this->manager->getRepository(\AppBundle\Entity\Genre::getBundle())->findOneBy($conditions);

                if (! isset($g)) {
                    $genreBuilder = new \AppBundle\Entity\Builders\Entity\MasterTable\GenreBuilder($this->manager, \AppBundle\Entity\Genre::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\MasterTable\MasterTableBuilder($genreBuilder);
                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                    $g = $entityUtils->add($this->manager, $genre, $g, $builder, $genre['code'], $genre['title'], $genre['description']);
                }

                $this->entity->__add($g, 'genres');
            }
        }
    }

}