<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMedia;

use AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaSeriesBuilder;
use AppBundle\Entity\Builders\Entity\EntityBuilder;

class SeriesBuilder extends EntityBuilder implements IContentMediaSeriesBuilder
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder::set()
     */
    public function set($title, $synopsis, $year, $country, $yearEnd = NULL, $duration = NULL)
    {
        $this->entity->setTitle($title)
            ->setSynopsis($synopsis)
            ->setYear($year)
            ->setCountry($country);

        if (isset($yearEnd)) {
            $this->entity->setYearEnd($yearEnd);
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder::addCasting()
     */
    public function addCasting($casts)
    {
        if (! empty($casts)) {
            foreach ($casts as $cast) {
                if (is_object($cast)) {
                    $cast = \AppBundle\Util\Utils::getInstance()->convertToArray($cast);
                }
                $conditions = array(
                    'name' => $cast['name'],
                    'surname' => $cast['surname']
                );

                $c = $this->manager->getRepository(\AppBundle\Entity\Cast::getBundle())->findOneBy($conditions);

                if (! isset($c)) {
                    $creditBuilder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\CastBuilder($this->manager, \AppBundle\Entity\Cast::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\ContentMediaCreditBuilder($creditBuilder);
                    $birthDate = (isset($cast['birth_date'])) ? \AppBundle\Util\Utils::getInstance()->createDate($cast['birth_date']) : NULL;

                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();

                    if (isset($cast['characters'])) {
                        $c = $entityUtils->add($this->manager, $cast, $c, $builder, $cast['name'], $cast['surname'], $cast['biography'], $birthDate, $cast['types_cast'], $cast['characters']);
                    } else {
                        $c = $entityUtils->add($this->manager, $cast, $c, $builder, $cast['name'], $cast['surname'], $cast['biography'], $birthDate, $cast['types_cast']);
                    }
                }

                $this->entity->__add($c, 'casts');
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder::addCharacters()
     */
    public function addCharacters($characters)
    {
        if (! empty($characters)) {
            foreach ($characters as $character) {
                if (is_object($character)) {
                    $character = \AppBundle\Util\Utils::getInstance()->convertToArray($character);
                }
                $conditions = array(
                    'name' => $character['name']
                );

                if (isset($character['surname'])) {
                    $conditions['surname'] = $character['surname'];
                }

                $c = $this->manager->getRepository(\AppBundle\Entity\Character::getBundle())->findOneBy($conditions);

                if (! isset($c)) {
                    $creditBuilder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\CharacterBuilder($this->manager, \AppBundle\Entity\Character::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\ContentMediaCreditBuilder($creditBuilder);

                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                    $c = $entityUtils->add($this->manager, $character, $c, $builder, $character['name'], $character['surname'], $character['biography']);
                }

                $this->entity->__add($c, 'characters');
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaBuilder::addGenres()
     */
    public function addGenres($genres)
    {
        if (! empty($genres)) {
            foreach ($genres as $genre) {
                $conditions = array(
                    'code' => $genre['code']
                );
                $g = $this->manager->getRepository(\AppBundle\Entity\Genre::getBundle())->findOneBy($conditions);

                if (! isset($g)) {
                    $genreBuilder = new \AppBundle\Entity\Builders\Entity\MasterTable\GenreBuilder($this->manager, \AppBundle\Entity\Genre::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\MasterTable\MasterTableBuilder($genreBuilder);
                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                    $g = $entityUtils->add($this->manager, $genre, $g, $builder, $genre['code'], $genre['title'], $genre['description']);
                }

                $this->entity->__add($g, 'genres');
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaSeriesBuilder::addStateSeries()
     */
    public function addStateSeries($states)
    {
        if (! empty($states)) {
            foreach ($states as $state) {
                $conditions = array(
                    'code' => $state['code']
                );
                $s = $this->manager->getRepository(\AppBundle\Entity\StateSeries::getBundle())->findOneBy($conditions);

                if (! isset($s)) {
                    $statesBuilder = new \AppBundle\Entity\Builders\Entity\MasterTable\StateSeriesBuilder($this->manager, \AppBundle\Entity\StateSeries::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\MasterTable\MasterTableBuilder($statesBuilder);
                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                    $s = $entityUtils->add($this->manager, $state, $s, $builder, $state['code'], $state['title'], $state['description']);
                }

                $this->entity->__add($s, 'statesSeries');
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaSeriesBuilder::addChapters()
     */
    public function addChapters($chapters)
    {
        if (! empty($chapters)) {
            foreach ($chapters as $chapter) {
                $chapterBuilder = new \AppBundle\Entity\Builders\Entity\ContentMedia\ChapterBuilder($this->manager, \AppBundle\Entity\Chapter::class);
                $builder = new \AppBundle\Entity\Builders\Entity\ContentMedia\ContentMediaChapterBuilder($chapterBuilder);
                $dateOfIssue = (isset($chapter['date_of_issue'])) ? \AppBundle\Util\Utils::getInstance()->createDate($chapter['date_of_issue']) : NULL;
                $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                $c = NULL;
                if (isset($chapter['casts']) && isset($chapter['characters'])) {
                    $c = $entityUtils->add($this->manager, $chapter, $c, $builder, $chapter['title'], $chapter['synopsis'], $chapter['duration'], $chapter['weight'], $dateOfIssue, $chapter['casts'], $chapter['characters']);
                } elseif (isset($chapter['casts'])) {
                    $c = $entityUtils->add($this->manager, $chapter, $c, $builder, $chapter['title'], $chapter['synopsis'], $chapter['duration'], $chapter['weight'], $dateOfIssue, $chapter['casts']);
                } else {
                    $c = $entityUtils->add($this->manager, $chapter, $c, $builder, $chapter['title'], $chapter['synopsis'], $chapter['duration'], $chapter['weight'], $dateOfIssue);
                }
                $this->entity->__add($c, 'chapters');
            }
            ;
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaSeriesBuilder::addSeasons()
     */
    public function addSeasons($seasons)
    {
        if (! empty($seasons)) {
            foreach ($seasons as $season) {
                $conditions = array(
                    'code' => $season['code']
                );
                $s = $this->manager->getRepository(\AppBundle\Entity\Season::getBundle())->findOneBy($conditions);

                if (! isset($s)) {
                    $seasonBuilder = new \AppBundle\Entity\Builders\Entity\MasterTable\SeasonBuilder($this->manager, \AppBundle\Entity\Season::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\MasterTable\MasterTableBuilder($seasonBuilder);
                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                    $s = $entityUtils->add($this->manager, $season, $s, $builder, $season['code'], $season['title'], $season['description']);
                }

                $this->entity->__add($s, 'seasons');
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\IBasicEntityBuilder::persist()
     */
    public function persist()
    {
        if (parent::persist()) {
            $chapters = $this->entity->getChapters();

            foreach ($chapters as $chapter) {
                $this->addCasting($chapter->getCasts());
                $this->addCharacters($chapter->getCharacters());
            }

            return parent::persist();
        }

        return FALSE;
    }

}