<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMedia;

use AppBundle\Entity\Builders\Entity\BasicBuilder;

class ContentMediaBuilder extends BasicBuilder
{

    /**
     * Content media builder.
     *
     * @param string $title
     *            Title.
     * @param string $synopsis
     *            Synopsis.
     * @param string $year
     *            Year.
     * @param string $country
     *            Country.
     * @param array $genres
     *            Genres.
     * @param array $casting
     *            Casting.
     * @param array $characters
     *            Characters.
     * @param string $yearEnd
     *            End year.
     */
    public function build($title, $synopsis, $year, $country, $genres = array(), $casting = array(), $characters = array(), $yearEnd = NULL, $duration = NULL)
    {
        $this->builder->set($title, $synopsis, $year, $country, $yearEnd, $duration);

        if (! empty($genres)) {
            $this->builder->addGenres($genres);
        }

        if (! empty($casting)) {
            $this->builder->addCasting($casting);
        }

        if (! empty($characters)) {
            $this->builder->addCharacters($characters);
        }

        return $this->builder->get();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Entity\BasicBuilder::getTranlatableProperties()
     */
    public function getTranlatableProperties()
    {
        return array(
            'title',
            'synopsis'
        );
    }

}