<?php
/**
 * @file
 * Builder.
 */
namespace AppBundle\Entity\Builders\Entity\ContentMedia;

use AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaChapterBuilder;
use AppBundle\Entity\Builders\Entity\EntityBuilder;

class ChapterBuilder extends EntityBuilder implements IContentMediaChapterBuilder
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaChapterBuilder::set()
     */
    public function set($title, $synopsis, $duration, $weight, $dateOfIssue = NULL)
    {
        $this->entity->setTitle($title)
            ->setSynopsis($synopsis)
            ->setDuration($duration)
            ->setWeight($weight)
            ->setDateOfIssue($dateOfIssue);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaChapterBuilder::addCasting()
     */
    public function addCasting($casts)
    {
        if (! empty($casts)) {
            foreach ($casts as $cast) {
                if (is_object($cast)) {
                    $cast = \AppBundle\Util\Utils::getInstance()->convertToArray($cast);
                }
                $conditions = array(
                    'name' => $cast['name'],
                    'surname' => $cast['surname']
                );

                $c = $this->manager->getRepository(\AppBundle\Entity\Cast::getBundle())->findOneBy($conditions);

                if (! isset($c)) {
                    $creditBuilder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\CastBuilder($this->manager, \AppBundle\Entity\Cast::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\ContentMediaCreditBuilder($creditBuilder);
                    $birthDate = (isset($cast['birth_date'])) ? \AppBundle\Util\Utils::getInstance()->createDate($cast['birth_date']) : NULL;

                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();

                    if (isset($cast['characters'])) {
                        $c = $entityUtils->add($this->manager, $cast, $c, $builder, $cast['name'], $cast['surname'], $cast['biography'], $birthDate, $cast['types_cast'], $cast['characters']);
                    } else {
                        $c = $entityUtils->add($this->manager, $cast, $c, $builder, $cast['name'], $cast['surname'], $cast['biography'], $birthDate, $cast['types_cast']);
                    }
                }

                $this->entity->__add($c, 'casts');
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Entity\Builders\Common\ContentMedia\IContentMediaChapterBuilder::addCharacters()
     */
    public function addCharacters($characters)
    {
        if (! empty($characters)) {
            foreach ($characters as $character) {
                if (is_object($character)) {
                    $character = \AppBundle\Util\Utils::getInstance()->convertToArray($character);
                }
                $conditions = array(
                    'name' => $character['name']
                );

                if (isset($character['surname'])) {
                    $conditions['surname'] = $character['surname'];
                }

                $c = $this->manager->getRepository(\AppBundle\Entity\Character::getBundle())->findOneBy($conditions);

                if (! isset($c)) {
                    $creditBuilder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\CharacterBuilder($this->manager, \AppBundle\Entity\Character::class);
                    $builder = new \AppBundle\Entity\Builders\Entity\ContentMediaCredit\ContentMediaCreditBuilder($creditBuilder);

                    $entityUtils = \AppBundle\Entity\Utils\EntityUtils::getInstance();
                    $c = $entityUtils->add($this->manager, $character, $c, $builder, $character['name'], $character['surname'], $character['biography']);
                }

                $this->entity->__add($c, 'characters');
            }
        }
    }

}