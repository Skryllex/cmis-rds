<?php
/**
 * @file
 * Entity Genre.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableMasterTableEntity;

/**
 * Genre.
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_genre", columns={"code", "title"})
 *   },
 *   indexes={
 *      @ORM\Index(name="genre_index", columns={"code", "title"}),
 * })
 * @ORM\Entity()
 */
final class Genre extends AbstractTranslatableMasterTableEntity
{

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Series", mappedBy="genres")
     */
    private $series;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Movie", mappedBy="genres")
     */
    private $movie;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Documentary", mappedBy="genres")
     */
    private $documentary;

    /**
     * Get series
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Get movie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Get documentary
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocumentary()
    {
        return $this->documentary;
    }
}
