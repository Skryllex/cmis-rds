<?php
/**
 * @file
 * Entity TypeBroadCastPlatform.
 */
namespace AppBundle\Entity;

use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractTranslatableMasterTableEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeBroadCastPlatform.
 *
 * @ORM\Table(
 *   uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_type_broadcast_platform", columns={"code", "title"})
 *   },
 *   indexes={
 *      @ORM\Index(name="type_broadcast_platform_index", columns={"code", "title"}),
 * })
 * @ORM\Entity()
 */
final class TypeBroadCastPlatform extends AbstractTranslatableMasterTableEntity
{
    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\BroadcastPlatform", mappedBy="typesBroadCastPlatform")
     */
    private $broadcastPlatform;

    /**
     * Get cast.
     */
    public function getBroadcastPlatform()
    {
        return $this->broadcastPlatform;
    }

}