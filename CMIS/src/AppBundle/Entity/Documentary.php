<?php
/**
 * @file
 * Entity Documentary.
 */
namespace AppBundle\Entity;

use AppBundle\Entity\Common\AbstractContentMedia;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entity Documentary
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="DOCUMENTARY_IDX", columns={"year", "title"}),
 * })
 * @ORM\Entity()
 */
final class Documentary extends AbstractContentMedia
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=5)
     */
    private $duration;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gallery", mappedBy="documentary", cascade={"all"})
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     * })
     */
    private $gallery;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Genre", inversedBy="documentary", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="documentary_genres")
     */
    private $genres;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Cast", inversedBy="documentary", cascade={"persist", "merge", "detach", "refresh"})
     * @ORM\JoinTable(name="documentary_credits")
     */
    private $casts;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\BroadcastPlatform", mappedBy="documentaries")
     */
    private $broadcastPlatforms;

    /**
     * Default contructor.
     */
    public function __construct()
    {
        $this->gallery = new \Doctrine\Common\Collections\ArrayCollection();
        $this->genres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->casts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->broadcastPlatforms = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return Documentary
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Documentary
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Add gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return Documentary
     */
    public function addGallery(\AppBundle\Entity\Gallery $gallery)
    {
        $this->gallery->add($gallery);

        return $this;
    }

    /**
     * Remove gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGallery(\AppBundle\Entity\Gallery $gallery)
    {
        return $this->gallery->removeElement($gallery);
    }

    /**
     * Get gallery
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Add genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return Documentary
     */
    public function addGenre(\AppBundle\Entity\Genre $genre)
    {
        $this->genres->add($genre);

        return $this;
    }

    /**
     * Remove genre
     *
     * @param \AppBundle\Entity\Genre $genre
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGenre(\AppBundle\Entity\Genre $genre)
    {
        return $this->genres->removeElement($genre);
    }

    /**
     * Get genres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Add cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return Documentary
     */
    public function addCast(\AppBundle\Entity\Cast $cast)
    {
        $this->casts->add($cast);

        return $this;
    }

    /**
     * Remove cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCast(\AppBundle\Entity\Cast $cast)
    {
        return $this->casts->removeElement($cast);
    }

    /**
     * Get casts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCasts()
    {
        return $this->casts;
    }

    /**
     * Get broadcastPlatforms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBroadcastPlatforms()
    {
        return $this->broadcastPlatforms;
    }

}
