<?php

/**
 * This file is part of the <name> project.
 *
 * (c) <yourname> <youremail>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\MediaBundle\Entity\BaseMedia as BaseMedia;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\GalleryHasMedia;

/**
 * Media
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="MEDIA_IDX", columns={"name"}),
 * })
 * @ORM\Entity()
 * @DoctrineAssert\UniqueEntity(fields={"name"})
 */
final class Media extends BaseMedia
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \AppBundle\Entity\Cast
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cast", inversedBy="images")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="cast", referencedColumnName="id")
     * })
     */
    private $cast;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="images")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="user", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->galleryHasMedias = new ArrayCollection();
    }

    /**
     * Non-PHPdoc.
     *
     * @see \Sonata\MediaBundle\Model\MediaInterface::getId()
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Media
     */
    public function setUser(User $user = NULL)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add galleryHasMedia
     *
     * @param \AppBundle\Entity\GalleryHasMedia $galleryHasMedia
     *
     * @return Media
     */
    public function addGalleryHasMedia(GalleryHasMedia $galleryHasMedia)
    {
        $this->galleryHasMedias[] = $galleryHasMedia;

        return $this;
    }

    /**
     * Remove galleryHasMedia
     *
     * @param \AppBundle\Entity\GalleryHasMedia $galleryHasMedia
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGalleryHasMedia(GalleryHasMedia $galleryHasMedia)
    {
        return $this->galleryHasMedias->removeElement($galleryHasMedia);
    }

    /**
     * Set cast
     *
     * @param \AppBundle\Entity\Cast $cast
     *
     * @return Media
     */
    public function setCast(\AppBundle\Entity\Cast $cast = null)
    {
        $this->cast = $cast;

        return $this;
    }

    /**
     * Get cast
     *
     * @return \AppBundle\Entity\Cast
     */
    public function getCast()
    {
        return $this->cast;
    }
}
