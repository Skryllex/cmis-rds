<?php
/**
 * @file
 * Entities tests.
 */
namespace AppBundle\Tests\Entity;

use AppBundle\Entity\BroadcastPlatform;
use AppBundle\Entity\Builders\Entity\BroadCastPlatform;
use AppBundle\Entity\Enum\TypeBroadCastPlatform as ETypeBroadCastPlatform;
use AppBundle\Entity\TypeBroadCastPlatform;
use AppBundle\Tests\Entity\Common\ICRUDEntitiesTest;
use AppBundle\Util\Enum as Common;
use AppBundle\Util\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class CRUDBroadCastPlatformTest extends WebTestCase implements ICRUDEntitiesTest
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::createTestData()
     */
    public function createTestData()
    {
        $data = array(
            array(
                'className' => BroadcastPlatform::class,
                'bundle' => BroadcastPlatform::getBundle(),
                'name' => 'British Broadcasting Corporation (BBC)',
                'description' => "The British Broadcasting Corporation (BBC) is the public-service broadcaster of the United Kingdom, headquartered at Broadcasting House in London. It is the world's oldest national broadcasting organisation[3] and the second largest broadcaster in the world by number of employees, with over 20,000 staff in total, of which 16,672 are in public sector broadcasting. The BBC is established under a Royal Charter and operates under its Agreement with the Secretary of State for Culture, Media and Sport. Its work is funded principally by an annual television licence fee which is charged to all British households, companies, and organisations using any type of equipment to receive or record live television broadcasts. The fee is set by the British Government, agreed by Parliament, and used to fund the BBC's extensive radio, TV, and online services covering the nations and regions of the UK. From 1 April 2014 it also funds the BBC World Service, launched in 1932, which provides comprehensive TV, radio, and online services in Arabic, and Persian, and broadcasts in 28 languages. Around a quarter of BBC revenues come from its commercial arm BBC Worldwide Ltd. which sells BBC programmes and services internationally and also distributes the BBC's international 24-hour English language news services BBC World News and BBC.com, provided by BBC Global News Ltd.",
                'translation' => array(
                    'es' => array(
                        'description' => "La British Broadcasting Corporation (BBC) es el organismo de radiodifusión de servicio público del Reino Unido, con sede en Broadcasting House en Londres. Es la organización más antigua del mundo de radiodifusión nacional [3] y el locutor de segundo más grande del mundo por número de empleados, con más de 20.000 empleados en total, de los cuales 16.672 están en la radiodifusión del sector público. La BBC se establece bajo una Carta Real y opera bajo su acuerdo con el Secretario de Estado de Cultura, Medios y Deporte. Su trabajo es financiado principalmente por una cuota anual de licencia de televisión que se cobra a todos los británicos hogares, empresas y organizaciones que utilizan cualquier tipo de equipo para recibir o grabar las emisiones de televisión en directo. La tarifa es fijada por el Gobierno británico, acordado por el Parlamento, y se utiliza para financiar la radio amplio, TV de la BBC, y los servicios en línea que cubre las naciones y regiones del Reino Unido. Del 1 de abril 2014 también financia el Servicio Mundial de la BBC, lanzado en 1932, que ofrece TV integral, la radio y los servicios en línea en árabe y persa, y las emisiones en 28 idiomas. Alrededor de una cuarta parte de la BBC ingresos provienen de su brazo comercial de la BBC Worldwide Ltd., que vende programas y servicios de la BBC a nivel internacional y también distribuye idioma Inglés 24 horas los servicios de noticias internacionales de la BBC BBC World News y BBC.com, proporcionados por la BBC Global News Ltd."
                    )
                ),
                'start_transmission' => '1992-10-18',
                'webSite' => 'http://www.bbc.com',
                'slogan' => NULL,
                'conditions' => array(
                    'name' => 'British Broadcasting Corporation (BBC)'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::create()
     * @test
     */
    public function create()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();
        $data = $this->createTestData();
        if (! $data->isEmpty()) {
            foreach ($data->toArray() as $value) {
                $networkBuilder = new BroadCastPlatform\BroadCastTelevisionNetworkBuilder($em, $value['className']);
                $builder = new BroadCastPlatform\BroadCastPlatformBuilder($networkBuilder);
                $object = $builder->build($value['name'], $value['description'], $value['webSite'], $value['slogan'], Utils::getInstance()->createDate($value['start_transmission']));
                $object = $builder->addTranslation($object, $em, $value['translation']);
                $result = $builder->getBuilder()->persist();
                $this->assertTrue($result);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::readTestData()
     */
    public function readTestData()
    {
        $data = array(
            array(
                'className' => BroadcastPlatform::class,
                'bundle' => BroadcastPlatform::getBundle(),
                'conditions' => array(
                    'name' => 'British Broadcasting Corporation (BBC)'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::read()
     * @test
     */
    public function read()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();

        $this->assertTrue((! $data->isEmpty()));

        foreach ($data->toArray() as $value) {
            $type = $em->getRepository(TypeBroadCastPlatform::getBundle())->findOneBy(array(
                'code' => ETypeBroadCastPlatform::BroadcastTelevisionNetwork
            ));

            $expr = new Expr();
            $query = array(
                'bundle' => $value['bundle'],
                'relationships' => array(
                    'typesBroadCastPlatform' => array(
                        'entity' => $type->getBundle(),
                        'alias' => 'type',
                        'condition_type' => Expr\Join::WITH,
                        'conditions' => $expr->eq('rel.typesBroadCastPlatform', 'type.id')
                    )
                ),
                'conditions' => $expr->orX($expr->eq('type.id', $type->getId()))
            );
            $readData = $em->getRepository($value['bundle'])->findByRelationship($query);

            $this->assertInstanceOf($value['className'], $readData);

            foreach ($value['conditions'] as $field => $value) {
                $property = 'get' . ucwords($field);
                $message = 'The data does not match';
                $this->assertSame($value, $readData->$property(), $message);
            }

            $message = 'The data does not match';
            $this->assertSame($type, $readData->getTypesBroadCastPlatform(), $message);
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::update()
     * @test
     */
    public function update()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $type = $em->getRepository(TypeBroadCastPlatform::getBundle())->findOneBy(array(
                'code' => ETypeBroadCastPlatform::BroadcastTelevisionNetwork
            ));

            $expr = new Expr();
            $query = array(
                'bundle' => $value['bundle'],
                'relationships' => array(
                    'typesBroadCastPlatform' => array(
                        'entity' => $type->getBundle(),
                        'alias' => 'type',
                        'condition_type' => Expr\Join::WITH,
                        'conditions' => $expr->eq('rel.typesBroadCastPlatform', 'type.id')
                    )
                ),
                'conditions' => $expr->orX($expr->eq('type.id', $type->getId()))
            );
            $readData = $em->getRepository($value['bundle'])->findByRelationship($query);

            $this->assertInstanceOf($value['className'], $readData);

            $readData->setName($readData->getName() . 'PO');

            try {
                $em->persist($readData);
                $em->flush();

                $expr = new Expr();
                $query = array(
                    'bundle' => $value['bundle'],
                    'relationships' => array(
                        'typesBroadCastPlatform' => array(
                            'entity' => $type->getBundle(),
                            'alias' => 'type',
                            'condition_type' => Expr\Join::WITH,
                            'conditions' => $expr->eq('rel.typesBroadCastPlatform', 'type.id')
                        )
                    ),
                    'conditions' => $expr->orX($expr->eq('type.id', $type->getId()))
                );
                $obModificated = $em->getRepository($value['bundle'])->findByRelationship($query);

                $insertedDate = $obModificated->getModificationDate();
                $message = 'The date not be modificated ' . $insertedDate->format('Y-m-d');
                $now = new \DateTime();
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::delete()
     * @test
     */
    public function delete()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $type = $em->getRepository(TypeBroadCastPlatform::getBundle())->findOneBy(array(
                'code' => ETypeBroadCastPlatform::BroadcastTelevisionNetwork
            ));

            $expr = new Expr();
            $query = array(
                'bundle' => $value['bundle'],
                'relationships' => array(
                    'typesBroadCastPlatform' => array(
                        'entity' => $type->getBundle(),
                        'alias' => 'type',
                        'condition_type' => Expr\Join::WITH,
                        'conditions' => $expr->eq('rel.typesBroadCastPlatform', 'type.id')
                    )
                ),
                'conditions' => $expr->orX($expr->eq('type.id', $type->getId()))
            );
            $readData = $em->getRepository($value['bundle'])->findByRelationship($query);

            $this->assertInstanceOf($value['className'], $readData);

            try {
                $this->assertTrue($em->getFilters()
                    ->isEnabled(Common\Gedmo::FilterSoftDeleteable));
                $em->remove($readData);
                $em->flush();

                $em->getFilters()->disable(Common\Gedmo::FilterSoftDeleteable);
                $expr = new Expr();
                $query = array(
                    'bundle' => $value['bundle'],
                    'relationships' => array(
                        'typesBroadCastPlatform' => array(
                            'entity' => $type->getBundle(),
                            'alias' => 'type',
                            'condition_type' => Expr\Join::WITH,
                            'conditions' => $expr->eq('rel.typesBroadCastPlatform', 'type.id')
                        )
                    ),
                    'conditions' => $expr->orX($expr->eq('type.id', $type->getId()))
                );
                $obModificated = $em->getRepository($value['bundle'])->findByRelationship($query);
                $insertedDate = $obModificated->getEndDate();
                $now = new \DateTime();
                $message = 'The data is not removed.';
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }
}