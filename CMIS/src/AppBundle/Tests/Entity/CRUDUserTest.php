<?php
/**
 * @file
 * Entities tests.
 */
namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Builders\Entity\User as Builder;
use AppBundle\Entity\Enum\Gender;
use AppBundle\Entity\Enum\MasterTableCode;
use AppBundle\Entity\Enum\Role;
use AppBundle\Entity\Enum\StateUser;
use AppBundle\Entity\Enum\UserManager;
use AppBundle\Entity\User;
use AppBundle\Tests\Entity\Common\ICRUDEntitiesTest;
use AppBundle\Util\Enum as Common;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CRUDUserTest extends WebTestCase implements ICRUDEntitiesTest
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::createTestData()
     */
    public function createTestData()
    {
        $data = array(
            array(
                'className' => User::class,
                'bundle' => User::getBundle(),
                'username' => 'sylar.sykes',
                'email' => "sylar.sykes@gmail.com",
                'password' => '816PT#TP618',
                'firstname' => 'Juan',
                'lastname' => 'González Fernández',
                'birth_date' => '1987-06-29',
                'gender' => Gender::Male,
                'address' => 'Avda. de Arteixo, 118',
                'postcode' => '15007',
                'city' => 'A Coruña',
                'country' => 'ES',
                'website' => '',
                'enabled' => StateUser::Enabled,
                'roles' => array(
                    Role::SuperAdministrator
                ),
                'groups' => array(
                    0 => array(
                        'code' => MasterTableCode::Group . 'SP'
                    )
                ),
                'conditions' => array(
                    'name' => 'Juan',
                    'username' => 'sylar.sykes'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::create()
     * @test
     */
    public function create()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $userManager = $client->getContainer()->get(UserManager::FOSUserManager);

        $data = $this->createTestData();
        if (! $data->isEmpty()) {
            foreach ($data->toArray() as $value) {
                $userBuilder = new Builder\UserBuilder($em, $userManager);
                $builder = new Builder\AppUserBuilder($userBuilder);
                $object = $builder->build($value['username'], $value['email'], $value['password'], $value['firstname'], $value['lastname'], $value['birth_date'], $value['gender'], $value['address'], $value['postcode'], $value['city'], $value['country'], $value['website'], $value['enabled'], $value['roles'], $value['groups']);
                $result = $builder->getBuilder()->persist();
                $this->assertTrue($result);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::readTestData()
     */
    public function readTestData()
    {
        $data = array(
            array(
                'className' => User::class,
                'bundle' => User::getBundle(),
                'conditions' => array(
                    'name' => 'Juan',
                    'username' => 'sylar.sykes'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::read()
     * @test
     */
    public function read()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();

        $this->assertTrue((! $data->isEmpty()));

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            foreach ($value['conditions'] as $field => $value) {
                $property = 'get' . ucwords($field);
                $message = 'The data does not match';
                $this->assertSame($value, $readData->$property(), $message);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::update()
     * @test
     */
    public function update()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            $name = $readData->getName() . 'PO';
            $readData->setName($name);

            try {
                $em->persist($readData);
                $em->flush();

                $value['conditions']['name'] = $name;
                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

                $insertedDate = $obModificated->getModificationDate();
                $message = 'The date not be modificated ' . $insertedDate->format('Y-m-d');
                $now = new \DateTime();
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::delete()
     * @test
     */
    public function delete()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            try {
                $this->assertTrue($em->getFilters()
                    ->isEnabled(Common\Gedmo::FilterSoftDeleteable));
                $em->remove($readData);
                $em->flush();

                $em->getFilters()->disable(Common\Gedmo::FilterSoftDeleteable);

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);
                $insertedDate = $obModificated->getEndDate();
                $now = new \DateTime();
                $message = 'The data is not removed.';
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }
}