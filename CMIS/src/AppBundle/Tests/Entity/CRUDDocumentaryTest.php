<?php
/**
 * @file
 * Entities tests.
 */
namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Builders\Entity\ContentMedia;
use AppBundle\Entity\Builders\Entity\MasterTable;
use AppBundle\Entity\Documentary;
use AppBundle\Tests\Entity\Common\ICRUDEntitiesTest;
use AppBundle\Util\Enum as Common;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class CRUDDocumentaryTest extends WebTestCase implements ICRUDEntitiesTest
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::createTestData()
     */
    public function createTestData()
    {
        $data = array(
            array(
                'className' => Documentary::class,
                'bundle' => Documentary::getBundle(),
                'title' => 'Citizenfour',
                'synopsis' => "In January 2013, Laura Poitras started receiving anonymous encrypted e-mails from 'Citizenfour', who claimed to have evidence of illegal covert surveillance programs run by the NSA in collaboration with other intelligence agencies worldwide. Five months later, she and reporters Glenn Greenwald and Ewen MacAskill flew to Hong Kong for the first of many meetings with the man who turned out to be Edward Snowden. She brought her camera with her. The resulting film is history unfolding before our eyes.",
                'translation' => array(
                    'es' => array(
                        'synopsis' => "En enero de 2013, Laura Poitras comenzó a recibir anónimos correos electrónicos cifrados de 'Citizenfour', quien afirmó tener pruebas de los programas de vigilancia encubierta ilegales dirigidas por la NSA en colaboración con otras agencias de inteligencia de todo el mundo. Cinco meses más tarde, ella y los periodistas Glenn Greenwald y Ewen MacAskill voló a Hong Kong para la primera de muchas reuniones con el hombre que resultó ser Edward Snowden. Ella trajo su cámara con ella. La película resultante es la historia se desarrolla ante nuestros ojos."
                    )
                ),
                'year' => '2014',
                'duration' => '114',
                'country' => 'United States',
                'genres' => array(
                    0 => array(
                        'code' => MasterTable\GenreBuilder::CODE_THRILLER
                    )
                ),
                'casts' => array(
                    0 => array(
                        'name' => 'Edward Joseph',
                        'surname' => 'Snowden',
                        'biography' => "Edward Joseph Snowden (born June 21, 1983) is an American privacy activist, computer professional, former CIA employee, and former government contractor who leaked classified information from the United States National Security Agency (NSA) in 2013. The information revealed numerous global surveillance programs, many run by the NSA and the Five Eyes with the cooperation of telecommunication companies and European governments.",
                        'translation' => array(
                            'es' => array(
                                'biography' => "Edward Joseph Snowden (nacido el 21 de junio de 1983) es un activista estadounidense privacidad, profesional de la informática, el ex empleado de la CIA, y el ex contratista del gobierno que filtró información clasificada de la Agencia de Seguridad Nacional de Estados Unidos (NSA) en 2013. La información reveló numerosos vigilancia mundial programas, muchos de ejecución por la NSA y los Cinco Ojos con la cooperación de las empresas de telecomunicaciones y los gobiernos europeos."
                            )
                        ),
                        'birth_date' => '1983-06-21',
                        'types_cast' => array(
                            0 => array(
                                'code' => MasterTable\TypeCastBuilder::CODE_ACTOR_ACTRESS
                            )
                        )
                    ),
                    1 => array(
                        'name' => 'Julian Paul',
                        'surname' => 'Assange',
                        'biography' => "Julian Paul Assange (born 3 July 1971) is an Australian computer programmer, publisher and journalist. He is known as the editor-in-chief of the website WikiLeaks, which he co-founded in 2006 after an earlier career in hacking and programming. WikiLeaks achieved particular prominence in 2010 when it published U.S. military and diplomatic documents leaked by Chelsea Manning. Assange has been under investigation in the United States since that time. In the same year, the Swedish Director of Public Prosecution opened an investigation into four sexual offences that Assange is alleged to have committed. In 2012, facing extradition to Sweden, he sought refuge at the Embassy of Ecuador in London and was granted political asylum by Ecuador.",
                        'translation' => array(
                            'es' => array(
                                'biography' => "Julian Paul Assange (nacido el 3 de julio de 1971) es un programador informático australiano, editor y periodista. Se le conoce como el editor en jefe del sitio web WikiLeaks, que él co-fundó en 2006 después de una carrera a principios de la piratería y la programación. WikiLeaks logra especial importancia en 2010 cuando publicó EE.UU. documentos militares y diplomáticos filtrados por Chelsea Manning. Assange ha sido objeto de investigación en los Estados Unidos desde entonces. En el mismo año, el director sueco del Ministerio Público abrió una investigación sobre cuatro delitos sexuales que Assange está acusado de haber cometido. En 2012, frente a la extradición a Suecia, se refugió en la embajada de Ecuador en Londres y se le concedió el asilo político por el Ecuador."
                            )
                        ),
                        'birth_date' => '1971-07-03',
                        'types_cast' => array(
                            0 => array(
                                'code' => MasterTable\TypeCastBuilder::CODE_ACTOR_ACTRESS
                            )
                        )
                    )
                ),
                'conditions' => array(
                    'title' => 'Citizenfour'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::create()
     * @test
     */
    public function create()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();
        $data = $this->createTestData();
        if (! $data->isEmpty()) {
            foreach ($data->toArray() as $value) {
                $networkBuilder = new ContentMedia\DocumentaryBuilder($em, $value['className']);
                $builder = new ContentMedia\ContentMediaBuilder($networkBuilder);
                $object = $builder->build($value['title'], $value['synopsis'], $value['year'], $value['country'], $value['genres'], $value['casts'], array(), NULL, $value['duration']);
                $object = $builder->addTranslation($object, $em, $value['translation']);
                $result = $builder->getBuilder()->persist();
                $this->assertTrue($result);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::readTestData()
     */
    public function readTestData()
    {
        $data = array(
            array(
                'className' => Documentary::class,
                'bundle' => Documentary::getBundle(),
                'conditions' => array(
                    'title' => 'Citizenfour'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::read()
     * @test
     */
    public function read()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();

        $this->assertTrue((! $data->isEmpty()));

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            foreach ($value['conditions'] as $field => $value) {
                $property = 'get' . ucwords($field);
                $message = 'The data does not match';
                $this->assertSame($value, $readData->$property(), $message);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::update()
     * @test
     */
    public function update()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            $readData->setTitle($readData->getTitle() . 'PO');

            try {
                $em->persist($readData);
                $em->flush();

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

                $insertedDate = $obModificated->getModificationDate();
                $message = 'The date not be modificated ' . $insertedDate->format('Y-m-d');
                $now = new \DateTime();
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::delete() @test
     */
    public function delete()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            try {
                $this->assertTrue($em->getFilters()
                    ->isEnabled(Common\Gedmo::FilterSoftDeleteable));
                $em->remove($readData);
                $em->flush();

                $em->getFilters()->disable(Common\Gedmo::FilterSoftDeleteable);

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);
                $insertedDate = $obModificated->getEndDate();
                $now = new \DateTime();
                $message = 'The data is not removed.';
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }
}