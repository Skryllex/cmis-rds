<?php
/**
 * @file
 * Master Table entities tests.
 */
namespace AppBundle\Tests\Entity\MasterTables;

use AppBundle\DataFixtures\ORM\Common\BasicMasterTableDataFixture;
use AppBundle\Entity;
use AppBundle\Entity\Enum\MasterTableCode;
use AppBundle\Tests\Entity\Common\ICRUDEntitiesTest;
use AppBundle\Util\Enum as Common;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class CRUDMasterTableTest extends WebTestCase implements ICRUDEntitiesTest
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::createTestData()
     */
    public function createTestData()
    {
        $data = array(
            // Data Genre.
            array(
                'className' => Entity\Genre::class,
                'bundle' => Entity\Genre::getBundle(),
                'code' => MasterTableCode::Genre . 'SH',
                'title' => 'Survival',
                'description' => 'Description survival',
                'conditions' => array(
                    'code' => MasterTableCode::Genre . 'SH',
                    'description' => 'Description survival'
                )
            ),
            // Data Quality.
            array(
                'className' => Entity\Quality::class,
                'bundle' => Entity\Quality::getBundle(),
                'code' => MasterTableCode::Quality . '240',
                'title' => 'SD-240p',
                'conditions' => array(
                    'code' => MasterTableCode::Quality . '240'
                )
            ),
            // Data Season.
            array(
                'className' => Entity\Season::class,
                'bundle' => Entity\Season::getBundle(),
                'code' => MasterTableCode::Season . 'S13',
                'title' => 'Season 13',
                'conditions' => array(
                    'code' => MasterTableCode::Season . 'S13'
                )
            ),
            // Data StateSeries.
            array(
                'className' => Entity\StateSeries::class,
                'bundle' => Entity\StateSeries::getBundle(),
                'code' => MasterTableCode::StateSeries . 'RE',
                'title' => 'Re-broadcasting',
                'conditions' => array(
                    'code' => MasterTableCode::StateSeries . 'RE'
                )
            ),
            // Data TypeCast.
            array(
                'className' => Entity\TypeCast::class,
                'bundle' => Entity\TypeCast::getBundle(),
                'code' => MasterTableCode::TypeCast . 'ST',
                'title' => 'Stuntman',
                'conditions' => array(
                    'code' => MasterTableCode::TypeCast . 'ST'
                )
            ),
            // Data TypeRepeatViewing.
            array(
                'className' => Entity\TypeRepeatViewing::class,
                'bundle' => Entity\TypeRepeatViewing::getBundle(),
                'code' => MasterTableCode::TypeRepeatViewing . 'ZZ',
                'title' => 'Once a two years',
                'conditions' => array(
                    'code' => MasterTableCode::TypeRepeatViewing . 'ZZ'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::create()
     * @test
     */
    public function create()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();
        $em->getConnection()->beginTransaction();
        $data = $this->createTestData();
        if (! $data->isEmpty()) {
            foreach ($data->toArray() as $value) {
                $object = BasicMasterTableDataFixture::getInstance()->createObject($value, $em);
                try {
                    $em->persist($object);
                    $em->flush();

                    $obInserted = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);
                    if ($obInserted) {
                        $message = 'The code value ' . $obInserted->getCode() . ' does not match with the value ' . $object->getCode();
                        $this->assertSame($object->getCode(), $obInserted->getCode(), $message);

                        $message = 'The title value ' . $obInserted->getTitle() . ' does not match with the value ' . $obInserted->getTitle();
                        $this->assertSame($object->getTitle(), $obInserted->getTitle(), $message);
                    }
                } catch (Exception $e) {
                    // Rollback the failed transaction attempt
                    $em->getConnection()->rollback();
                }
            }
            $em->getConnection()->rollback();
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::readTestData()
     */
    public function readTestData()
    {
        $data = array(
            // Data Genre.
            array(
                'className' => Entity\Genre::class,
                'bundle' => Entity\Genre::getBundle(),
                'conditions' => array(
                    'code' => MasterTableCode::Genre . 'AN',
                    'title' => 'Animation'
                )
            ),
            // Data Quality.
            array(
                'className' => Entity\Quality::class,
                'bundle' => Entity\Quality::getBundle(),
                'conditions' => array(
                    'code' => MasterTableCode::Quality . '480',
                    'title' => 'SD-480p'
                )
            ),
            // Data Season.
            array(
                'className' => Entity\Season::class,
                'bundle' => Entity\Season::getBundle(),
                'conditions' => array(
                    'code' => MasterTableCode::Season . 'S1',
                    'title' => 'Season 1'
                )
            ),
            // Data StateSeries.
            array(
                'className' => Entity\StateSeries::class,
                'bundle' => Entity\StateSeries::getBundle(),
                'conditions' => array(
                    'code' => MasterTableCode::StateSeries . 'IB',
                    'title' => 'In broadcasting'
                )
            ),
            // Data TypeCast.
            array(
                'className' => Entity\TypeCast::class,
                'bundle' => Entity\TypeCast::getBundle(),
                'conditions' => array(
                    'code' => MasterTableCode::TypeCast . 'DC',
                    'title' => 'Director'
                )
            ),
            // Data TypeRepeatViewing.
            array(
                'className' => Entity\TypeRepeatViewing::class,
                'bundle' => Entity\TypeRepeatViewing::getBundle(),
                'conditions' => array(
                    'code' => MasterTableCode::TypeRepeatViewing . 'NV',
                    'title' => 'Never'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::read()
     * @test
     */
    public function read()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();

        $this->assertTrue((! $data->isEmpty()));

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);
            $this->assertInstanceOf($value['className'], $readData);
            foreach ($value['conditions'] as $field => $value) {
                $property = 'get' . ucwords($field);
                $message = 'The data does not match';
                $this->assertSame($value, $readData->$property(), $message);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::update()
     * @test
     */
    public function update()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();
        $em->getConnection()->beginTransaction();
        $obInserted = $em->getRepository(Entity\Genre::getBundle())->findOneBy(array(
            'code' => MasterTableCode::Genre . 'AN'
        ));

        if ($obInserted) {
            $mCode = $obInserted->getCode() . 'P';
            $obInserted->setCode($mCode);
            try {
                $em->persist($obInserted);
                $em->flush();
                $obModificated = $em->getRepository(Entity\Genre::getBundle())->findOneBy(array(
                    'code' => $mCode
                ));
                $insertedDate = $obModificated->getModificationDate();
                $message = 'The date not be modificated ' . $insertedDate->format('Y-m-d');
                $now = new \DateTime();
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::delete()
     * @test
     */
    public function delete()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();
        $em->getConnection()->beginTransaction();
        $obInserted = $em->getRepository(Entity\Genre::getBundle())->findOneBy(array(
            'code' => MasterTableCode::Genre . 'AN'
        ));

        if ($obInserted) {
            try {
                $this->assertTrue($em->getFilters()
                    ->isEnabled(Common\Gedmo::FilterSoftDeleteable));
                $em->remove($obInserted);
                $em->flush();
                $em->getFilters()->disable(Common\Gedmo::FilterSoftDeleteable);
                $obModificated = $em->getRepository(Entity\Genre::getBundle())->findOneBy(array(
                    'code' => MasterTableCode::Genre . 'AN'
                ));
                $insertedDate = $obModificated->getEndDate();
                $now = new \DateTime();
                $message = 'The data is not removed.';
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }
}