<?php
/**
 * @file
 * Basic definition for the tests to entities.
 */
namespace AppBundle\Tests\Entity\Common;

interface ICRUDEntitiesTest
{

    /**
     * Definition of test data.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection Test data.
     */
    public function createTestData();

    /**
     * Definition of the test create data.
     */
    public function create();

    /**
     * Definition of an array with conditions for the read of the test data.
     */
    public function readTestData();

    /**
     * Definition of the test read data.
     */
    public function read();

    /**
     * Definition of the test update data.
     */
    public function update();

    /**
     * Definition of the test delete data.
     */
    public function delete();

}