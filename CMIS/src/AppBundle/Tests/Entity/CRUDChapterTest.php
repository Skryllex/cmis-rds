<?php
/**
 * @file
 * Entities tests.
 */
namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Builders\Entity\ContentMedia;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\Enum\TypeCast as EnumTypeCast;
use AppBundle\Tests\Entity\Common\ICRUDEntitiesTest;
use AppBundle\Util\Enum as Common;
use AppBundle\Util\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class CRUDChapterTest extends WebTestCase implements ICRUDEntitiesTest
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::createTestData()
     */
    public function createTestData()
    {
        $data = array(
            array(
                'className' => Chapter::class,
                'bundle' => Chapter::getBundle(),
                'title' => 'The End of the World',
                'synopsis' => 'The Doctor takes Rose to the year 5 billion to witness the destruction of the Earth.',
                'duration' => '45',
                'date_of_issue' => '2005-04-02',
                'weight' => '1',
                'translation' => array(
                    'es' => array(
                        'title' => 'El fin del mundo',
                        'synopsis' => 'El doctor toma Rose al año 5 mil millones para presenciar la destrucción de la Tierra.'
                    )
                ),
                'casts' => array(
                    0 => array(
                        'name' => 'Euros',
                        'surname' => 'Lyn',
                        'biography' => "Euros Lyn was born in 1971 in Wales. He is a director, known for Happy Valley (2014), Doctor Who (2005) and Doctor Who at the Proms (2009).",
                        'translations' => array(
                            'es' => array(
                                'biography' => "Euros Lyn nació en 1971 en Gales. Él es un director, conocido por Happy Valley (2014), Doctor Who (2005) y doctor Who en los Proms (2009)."
                            )
                        ),
                        'birth_date' => NULL,
                        'types_cast' => array(
                            0 => array(
                                'code' => EnumTypeCast::Director
                            )
                        )
                    ),
                    1 => array(
                        'name' => 'Russell T',
                        'surname' => 'Davies'
                    ),
                    2 => array(
                        'name' => 'Christopher',
                        'surname' => 'Eccleston'
                    ),
                    3 => array(
                        'name' => 'Billie',
                        'surname' => 'Piper'
                    ),
                    4 => array(
                        'name' => 'Simon Paisley',
                        'surname' => 'Day',
                        'biography' => "Simon Paisley Day was born on April 13, 1967 in Gillingham, Kent, England. He is an actor, known for La legión del águila (2011), Un plan brillante (2007) and Espartaco (2004).",
                        'translations' => array(
                            'es' => array(
                                'biography' => "Simon Day Paisley nació el 13 de abril de 1967 en Gillingham, Kent, Inglaterra. Él es un actor, conocido por La legión del águila (2011), brillante Un plan de (2007) y Espartaco (2004)."
                            )
                        ),
                        'birth_date' => '1967-04-13',
                        'types_cast' => array(
                            0 => array(
                                'code' => EnumTypeCast::ActorActress
                            )
                        ),
                        'characters' => array(
                            0 => array(
                                'name' => 'Steward',
                                'surname' => NULL,
                                'biography' => NULL
                            )
                        )
                    )
                ),
                'characters' => array(
                    0 => array(
                        'name' => 'The Doctor'
                    ),
                    1 => array(
                        'name' => 'Rose',
                        'surname' => 'Tyler'
                    )
                ),
                'conditions' => array(
                    'title' => 'The End of the World'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::create()
     * @test
     */
    public function create()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();
        $data = $this->createTestData();
        if (! $data->isEmpty()) {
            foreach ($data->toArray() as $value) {
                $seriesBuilder = new ContentMedia\ChapterBuilder($em, \AppBundle\Entity\Chapter::class);
                $builder = new ContentMedia\ContentMediaChapterBuilder($seriesBuilder);
                $object = $builder->build($value['title'], $value['synopsis'], $value['duration'], $value['weight'], Utils::getInstance()->createDate($value['date_of_issue']), $value['casts'], $value['characters']);
                $object = $builder->addTranslation($object, $em, $value['translation']);
                $result = $builder->getBuilder()->persist();
                $this->assertTrue($result);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::readTestData()
     */
    public function readTestData()
    {
        $data = array(
            array(
                'className' => Chapter::class,
                'bundle' => Chapter::getBundle(),
                'conditions' => array(
                    'title' => 'The End of the World'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::read()
     * @test
     */
    public function read()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();

        $this->assertTrue((! $data->isEmpty()));

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            foreach ($value['conditions'] as $field => $value) {
                $property = 'get' . ucwords($field);
                $message = 'The data does not match';
                $this->assertSame($value, $readData->$property(), $message);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::update()
     * @test
     */
    public function update()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            $title = $readData->getTitle() . 'PO';
            $readData->setTitle($title);

            try {
                $em->persist($readData);
                $em->flush();

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

                $insertedDate = $obModificated->getModificationDate();
                $message = 'The date not be modificated ' . $insertedDate->format('Y-m-d');
                $now = new \DateTime();
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::delete()
     * @test
     */
    public function delete()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            try {
                $this->assertTrue($em->getFilters()
                    ->isEnabled(Common\Gedmo::FilterSoftDeleteable));
                $em->remove($readData);
                $em->flush();

                $em->getFilters()->disable(Common\Gedmo::FilterSoftDeleteable);

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);
                $insertedDate = $obModificated->getEndDate();
                $now = new \DateTime();
                $message = 'The data is not removed.';
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }
}