To run these tests the fixtures must be loaded previously. 
Run the tests in the following order:

phpunit -c app src/AppBundle/Tests/Entity/MasterTables/CRUDMasterTableTest.php

phpunit -c app src/AppBundle/Tests/Entity/CRUDBroadcastPlatformTest.php

phpunit -c app src/AppBundle/Tests/Entity/CRUDCastTest.php

phpunit -c app src/AppBundle/Tests/Entity/CRUDCharacterTest.php

phpunit -c app src/AppBundle/Tests/Entity/CRUDDocumentaryTest.php

phpunit -c app src/AppBundle/Tests/Entity/CRUDSeriesTest.php

phpunit -c app src/AppBundle/Tests/Entity/CRUDChapterTest.php

phpunit -c app src/AppBundle/Tests/Entity/CRUDMovieTest.php

phpunit -c app src/AppBundle/Tests/Entity/CRUDUserTest.php