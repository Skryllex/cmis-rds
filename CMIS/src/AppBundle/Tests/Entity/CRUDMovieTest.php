<?php
/**
 * @file
 * Entities tests.
 */
namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Builders\Entity\ContentMedia;
use AppBundle\Entity\Enum\Genre as EnumGenre;
use AppBundle\Entity\Enum\TypeCast as EnumTypeCast;
use AppBundle\Entity\Movie;
use AppBundle\Tests\Entity\Common\ICRUDEntitiesTest;
use AppBundle\Util\Enum as Common;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CRUDMovieTest extends WebTestCase implements ICRUDEntitiesTest
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::createTestData()
     */
    public function createTestData()
    {
        $data = array(
            array(
                'className' => Movie::class,
                'bundle' => Movie::getBundle(),
                'title' => 'Doctor Who: The Movie',
                'synopsis' => "Doctor Who, also referred to as Doctor Who: The Movie to distinguish it from the television series of the same name, is a British-American-Canadian television film continuing the British science fiction television series Doctor Who. Developed as a co-production between BBC Worldwide, Universal Studios, 20th Century Fox and the American network Fox, the 1996 television film premiered on 12 May 1996 on CITV in Edmonton, Alberta, Canada (which was owned by WIC at the time before being acquired by Canwest Global in 2000), 15 days before its first showing in the United Kingdom on BBC One and two days before being broadcast in the United States on Fox. It was also shown in some countries for a limited time in cinemas.",
                'translation' => array(
                    'es' => array(
                        'synopsis' => "El doctor Who, también conocido como doctor Who: The Movie para distinguirla de la serie de televisión del mismo nombre, es una película de la televisión británica-estadounidense canadiense continuar el doctor serie de televisión de ciencia ficción británica Quién. Desarrollado como una co-producción entre BBC Worldwide, Universal Studios, 20th Century Fox y la cadena estadounidense Fox, la película de televisión 1.996 se estrenó el 12 de mayo de 1996 sobre CITV en Edmonton, Alberta, Canadá (que era propiedad de WIC en el momento antes de ser adquirida por Canwest Global en 2000), 15 días antes de su primera actuación en el Reino Unido en BBC One y dos días antes de ser transmitido en los Estados Unidos por Fox. También se demostró en algunos países por un tiempo limitado en los cines."
                    )
                ),
                'year' => '1996',
                'duration' => '89',
                'country' => 'Canada, United States, United Kingdom',
                'genres' => array(
                    0 => array(
                        'code' => EnumGenre::ScienceFiction
                    ),
                    1 => array(
                        'code' => EnumGenre::Adventure
                    ),
                    2 => array(
                        'code' => EnumGenre::Drama
                    )
                ),
                'casts' => array(
                    0 => array(
                        'name' => 'Geoffrey',
                        'surname' => 'Sax',
                        'biography' => "Geoffrey Sax (sometimes credited as Geoff Sax) is a British film and television director, who has worked on a variety of drama productions in both the UK and the United States.",
                        'translation' => array(
                            'es' => array(
                                'biography' => "Geoffrey Sax (a veces acreditado como Geoff Sax) es un director de cine y televisión británica, que ha trabajado en una variedad de producciones de teatro en el Reino Unido y los Estados Unidos."
                            )
                        ),
                        'birth_date' => NULL,
                        'types_cast' => array(
                            0 => array(
                                'code' => EnumTypeCast::Director
                            )
                        )
                    ),
                    1 => array(
                        'name' => 'Paul John',
                        'surname' => 'McGann',
                        'biography' => 'Paul John McGann (born 14 November 1959) is an English actor. He came to prominence for portraying Percy Toplis in the 1986 television serial The Monocled Mutineer. He later starred in the 1987 dark comedy Withnail and I, and as the eighth incarnation of the Doctor in the 1996 Doctor Who television film, a role he reprised in more than 70 audio dramas and the 2013 mini-episode "The Night of the Doctor".',
                        'translation' => array(
                            'es' => array(
                                'biography' => 'Paul John McGann (born 14 November 1959) is an English actor. He came to prominence for portraying Percy Toplis in the 1986 television serial The Monocled Mutineer. He later starred in the 1987 dark comedy Withnail and I, and as the eighth incarnation of the Doctor in the 1996 Doctor Who television film, a role he reprised in more than 70 audio dramas and the 2013 mini-episode "The Night of the Doctor".'
                            )
                        ),
                        'birth_date' => '1959-11-14',
                        'types_cast' => array(
                            0 => array(
                                'code' => EnumTypeCast::ActorActress
                            )
                        ),
                        'characters' => array(
                            0 => array(
                                'name' => 'The Doctor'
                            )
                        )
                    ),
                    2 => array(
                        'name' => 'Sylvester',
                        'surname' => 'McCoy',
                        'biography' => "Sylvester McCoy (born Percy James Patrick Kent-Smith: 20 August 1943) is a Scottish actor, best known for playing the seventh incarnation of the Doctor in the long-running science fiction television series Doctor Who from 1987 to 1989 – the final Doctor of the original run – and briefly returning in a television film in 1996 and for his role as the wizard Radagast the Brown in Peter Jackson's film adaptations of The Hobbit.",
                        'translation' => array(
                            'es' => array(
                                'biography' => "Sylvester McCoy (nacido Percy James Patrick Kent-Smith: 20 Agosto 1943) es un actor escocés, conocido por interpretar a la séptima encarnación del doctor en el doctor de la ciencia ficción serie de televisión de larga duración Quién 1987-1989 - Doctor final el funcionamiento original - y volviendo brevemente en una película para la televisión en 1996 y por su papel como el mago Radagast el Pardo, en las adaptaciones cinematográficas de Peter Jackson de el Hobbit."
                            )
                        ),
                        'birth_date' => '1943-08-20',
                        'types_cast' => array(
                            0 => array(
                                'code' => EnumTypeCast::ActorActress
                            )
                        ),
                        'characters' => array(
                            0 => array(
                                'name' => 'The Doctor'
                            )
                        )
                    ),
                    3 => array(
                        'name' => 'Daphne Lee',
                        'surname' => 'Ashbrook',
                        'biography' => "Daphne Lee Ashbrook (born January 30, 1963) is an American actress.",
                        'translation' => array(
                            'es' => array(
                                'biography' => "Daphne Ashbrook Lee (nacido el 30 de enero 1963) es una actriz estadounidense."
                            )
                        ),
                        'birth_date' => '1963-01-30',
                        'types_cast' => array(
                            0 => array(
                                'code' => EnumTypeCast::ActorActress
                            )
                        ),
                        'characters' => array(
                            0 => array(
                                'name' => 'Grace',
                                'surname' => 'Holloway',
                                'biography' => "Dr. Grace Holloway is a fictional character played by Daphne Ashbrook in the 1996 television movie Doctor Who, a continuation of the long-running British science fiction television series Doctor Who. A cardiologist from San Francisco in 1999, she assists the Eighth Doctor in defeating the renegade Time Lord, the Master.",
                                'translation' => array(
                                    'es' => array(
                                        'biography' => "Doctora Grace Holloway es un personaje de ficción interpretado por Daphne Ashbrook en el doctor película para televisión 1996 Quién, una continuación del doctor de larga duración británico de ciencia ficción serie de televisión ¿Quién. Un cardiólogo de San Francisco en 1999, ella asiste al Octavo Doctor en derrotar al renegado señor del tiempo, el Maestro."
                                    )
                                )
                            )
                        )
                    )
                ),
                'characters' => array(
                    0 => array(
                        'name' => 'The Doctor'
                    )
                ),
                'conditions' => array(
                    'title' => 'Doctor Who',
                    'year' => '2005'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::create()
     * @test
     */
    public function create()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->createTestData();
        if (! $data->isEmpty()) {
            foreach ($data->toArray() as $value) {
                $moviesBuilder = new ContentMedia\MovieBuilder($em, $value['className']);
                $builder = new ContentMedia\ContentMediaBuilder($moviesBuilder);
                $object = $builder->build($value['title'], $value['synopsis'], $value['year'], $value['country'], $value['genres'], $value['casts'], $value['characters'], NULL, $value['duration']);
                $object = $builder->addTranslation($object, $em, $value['translation']);
                $result = $builder->getBuilder()->persist();
                $this->assertTrue($result);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::readTestData()
     */
    public function readTestData()
    {
        $data = array(
            array(
                'className' => Movie::class,
                'bundle' => Movie::getBundle(),
                'conditions' => array(
                    'title' => 'Doctor Who',
                    'year' => '2005'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::read()
     * @test
     */
    public function read()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();

        $this->assertTrue((! $data->isEmpty()));

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            foreach ($value['conditions'] as $field => $value) {
                $property = 'get' . ucwords($field);
                $message = 'The data does not match';
                $this->assertSame($value, $readData->$property(), $message);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::update()
     * @test
     */
    public function update()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            $title = $readData->getTitle() . 'PO';
            $readData->setTitle($title);

            try {
                $em->persist($readData);
                $em->flush();

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

                $insertedDate = $obModificated->getModificationDate();
                $message = 'The date not be modificated ' . $insertedDate->format('Y-m-d');
                $now = new \DateTime();
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::delete()
     * @test
     */
    public function delete()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            try {
                $this->assertTrue($em->getFilters()
                    ->isEnabled(Common\Gedmo::FilterSoftDeleteable));
                $em->remove($readData);
                $em->flush();

                $em->getFilters()->disable(Common\Gedmo::FilterSoftDeleteable);

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);
                $insertedDate = $obModificated->getEndDate();
                $now = new \DateTime();
                $message = 'The data is not removed.';
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }
}