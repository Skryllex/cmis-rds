<?php
/**
 * @file
 * Entities tests.
 */
namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Builders\Entity\ContentMediaCredit;
use AppBundle\Entity\Cast;
use AppBundle\Entity\Enum\TypeCast as EnumTypeCast;
use AppBundle\Entity\TypeCast;
use AppBundle\Tests\Entity\Common\ICRUDEntitiesTest;
use AppBundle\Util\Enum as Common;
use AppBundle\Util\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CRUDCastTest extends WebTestCase implements ICRUDEntitiesTest
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::createTestData()
     */
    public function createTestData()
    {
        $data = array(
            array(
                'className' => Cast::class,
                'bundle' => Cast::getBundle(),
                'name' => 'Peter Dougan',
                'surname' => 'Capaldi',
                'biography' => "Peter Dougan Capaldi was born in Glasgow, Scotland, to Nancy (Soutar) and Gerald John Capaldi. His parents owned an ice cream business. He is of Italian (from his paternal grandfather), Scottish, and Irish descent. Capaldi attended drama classes and was accepted into the Glasgow School of Art. While studying there he secured his breakthrough role in Un tipo genial (1983). He was announced the 12th Doctor Who 4th August 2013 on a BBC special programme. He had to hide it from his daughter who remarked to him why it is his name never came up during the buzz. It was a huge relief not to have to keep the secret anymore. His agent called and said 'Hello Doctor' when informing him he had gotten the part.",
                'translation' => array(
                    'es' => array(
                        'biography' => "Peter Capaldi Dougan nació en Glasgow, Escocia, con Nancy (Soutar) y Gerald John Capaldi. Sus padres tenían un negocio de helados. Él es de ascendencia italiana (de su abuelo paterno), escoceses e irlandeses. Capaldi asistió a clases de teatro y fue aceptado en la Escuela de Arte de Glasgow. Mientras estudiaba allí consiguió su primer papel importante en Un tipo genial (1983). Él fue anunciado el 12 de Doctor Who 04 de agosto 2013 en un programa especial de la BBC. Tuvo que esconderse de su hija, que le comentó por qué es su nombre nunca surgió durante el zumbido. Fue un gran alivio no tener que mantener el secreto. Su agente llamó y dijo 'Hola Doctor' cuando se le informaba que había conseguido el papel."
                    )
                ),
                'birth_date' => '1958-04-14',
                'types_cast' => array(
                    0 => array(
                        'code' => EnumTypeCast::ActorActress,
                    ),
                    1 => array(
                        'code' => EnumTypeCast::Director,
                    )
                ),
                'characters' => array(
                    0 => array(
                        'name' => 'The Doctor',
                        'surname' => NULL,
                        'biography' => "The Doctor is the last of an ancient, nearly God-like race called the Time Lords from the planet Gallifrey. He fled his home world with his granddaughter, Susan, to explore the universe. He travels in a living time machine known as the TARDIS (Time and Relative Dimension in Space), which he stole from the Time Lords and which appears like a small London police box on the outside but which has nearly infinite dimensions on the inside. After some initial adventures with Susan in 1963 London, their odd lifestyle attracted the attention of two schoolteachers who forced their way into the TARDIS. The Doctor, wishing to keep secret his and Susan's existence, essentially kidnapped the teachers and took them on a number of adventures, eventually returning them and traveling with other companions, including, more recently, Rose Tyler, Amy Pond, River Song, Martha Jones and Donna Noble. He is described as the universes greatest defender and has saved it countless times from all manner of alien, war or other disaster. He shows a soft spot for humans.",
                        'translation' => array(
                            'es' => array(
                                'biography' => "El Doctor es el último de una raza antigua, casi como Dios llamado el Tiempo Señores del planeta Gallifrey. Él huyó de su mundo natal con su nieta, Susan, para explorar el universo. Viaja en una máquina del tiempo la vida conocida como la TARDIS (Tiempo y Dimensión Relativa en el Espacio), que le robó a los Señores del Tiempo y que aparece como un pequeño cuadro de la policía de Londres en el exterior, pero que tiene unas dimensiones casi infinitas en el interior. Después de algunas aventuras iniciales con Susan en 1963 Londres, su estilo de vida extraño atrajo la atención de los dos maestros de escuela que forzaron su camino en la TARDIS. El doctor, con el deseo de mantener en secreto su existencia y de Susan, en esencia secuestró a los maestros y los llevó en una serie de aventuras, finalmente devolverlos y viajando con otros compañeros, incluyendo, más recientemente, Rose Tyler, Amy Pond, River Song, Martha Jones y Donna Noble. Se le describe como el mayor defensor universos y ha salvado innumerables veces de toda clase de extranjero, guerra u otro desastre. El autor muestra una debilidad por los seres humanos."
                            )
                        )
                    )
                ),
                'conditions' => array(
                    'name' => 'Peter Dougan',
                    'surname' => 'Capaldi'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::create()
     * @test
     */
    public function create()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();
        $data = $this->createTestData();
        if (! $data->isEmpty()) {
            foreach ($data->toArray() as $value) {
                $creditBuilder = new ContentMediaCredit\CastBuilder($em, $value['className']);
                $builder = new ContentMediaCredit\ContentMediaCreditBuilder($creditBuilder);
                $object = $builder->build($value['name'], $value['surname'], $value['biography'], Utils::getInstance()->createDate($value['birth_date']), $value['types_cast'], $value['characters']);
                $object = $builder->addTranslation($object, $em, $value['translation']);
                $result = $builder->getBuilder()->persist();
                $this->assertTrue($result);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::readTestData()
     */
    public function readTestData()
    {
        $data = array(
            array(
                'className' => Cast::class,
                'bundle' => Cast::getBundle(),
                'conditions' => array(
                    'name' => 'Peter Dougan',
                    'surname' => 'Capaldi'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::read()
     * @test
     */
    public function read()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();

        $this->assertTrue((! $data->isEmpty()));
        foreach ($data->toArray() as $value) {
            $types = $em->getRepository(TypeCast::getBundle())->findBy(array(
                'code' => array(
                    EnumTypeCast::ActorActress,
                    EnumTypeCast::Director
                )
            ));

            $expr = new Expr();
            $query = array(
                'bundle' => $value['bundle'],
                'relationships' => array(
                    'TypeCast' => array(
                        'entity' => TypeCast::getBundle(),
                        'alias' => 'type',
                        'condition_type' => Expr\Join::WITH,
                        'conditions' => $expr->eq('rel.id', 'type.id')
                    )
                ),
                'conditions' => array(
                    'or' => $expr->orX($expr->eq('type.id', $types[0]->getId()), $expr->eq('type.id', $types[1]->getId())),
                    'and' => $expr->andX($expr->eq('rel.name', $expr->literal($value['conditions']['name'])), $expr->eq('rel.surname', $expr->literal($value['conditions']['surname'])))
                )
            );
            $readData = $em->getRepository($value['bundle'])->findByRelationship($query);

            $this->assertInstanceOf($value['className'], $readData);

            foreach ($value['conditions'] as $field => $value) {
                $property = 'get' . ucwords($field);
                $message = 'The data does not match';
                $this->assertSame($value, $readData->$property(), $message);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::update()
     * @test
     */
    public function update()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $types = $em->getRepository(\AppBundle\Entity\TypeCast::getBundle())->findBy(array(
                'code' => array(
                    EnumTypeCast::ActorActress,
                    EnumTypeCast::Director
                )
            ));

            $expr = new Expr();
            $query = array(
                'bundle' => $value['bundle'],
                'relationships' => array(
                    'TypeCast' => array(
                        'entity' => TypeCast::getBundle(),
                        'alias' => 'type',
                        'condition_type' => Expr\Join::WITH,
                        'conditions' => $expr->eq('rel.id', 'type.id')
                    )
                ),
                'conditions' => array(
                    'or' => $expr->orX($expr->eq('type.id', $types[0]->getId()), $expr->eq('type.id', $types[1]->getId())),
                    'and' => $expr->andX($expr->eq('rel.name', $expr->literal($value['conditions']['name'])), $expr->eq('rel.surname', $expr->literal($value['conditions']['surname'])))
                )
            );
            $readData = $em->getRepository($value['bundle'])->findByRelationship($query);

            $this->assertInstanceOf($value['className'], $readData);

            $readData->setName($readData->getName() . 'PO');

            try {
                $em->persist($readData);
                $em->flush();

                $expr = new Expr();
                $query = array(
                    'bundle' => $value['bundle'],
                    'relationships' => array(
                        'TypeCast' => array(
                            'entity' => TypeCast::getBundle(),
                            'alias' => 'type',
                            'condition_type' => Expr\Join::WITH,
                            'conditions' => $expr->eq('rel.id', 'type.id')
                        )
                    ),
                    'conditions' => array(
                        'or' => $expr->orX($expr->eq('type.id', $types[0]->getId()), $expr->eq('type.id', $types[1]->getId())),
                        'and' => $expr->andX($expr->eq('rel.name', $expr->literal($value['conditions']['name'] . 'PO')), $expr->eq('rel.surname', $expr->literal($value['conditions']['surname'])))
                    )
                );
                $obModificated = $em->getRepository($value['bundle'])->findByRelationship($query);

                $insertedDate = $obModificated->getModificationDate();
                $message = 'The date not be modificated ' . $insertedDate->format('Y-m-d');
                $now = new \DateTime();
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::delete()
     * @test
     */
    public function delete()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $types = $em->getRepository(\AppBundle\Entity\TypeCast::getBundle())->findBy(array(
                'code' => array(
                    EnumTypeCast::ActorActress,
                    EnumTypeCast::Director
                )
            ));

            $expr = new Expr();
            $query = array(
                'bundle' => $value['bundle'],
                'relationships' => array(
                    'TypeCast' => array(
                        'entity' => TypeCast::getBundle(),
                        'alias' => 'type',
                        'condition_type' => Expr\Join::WITH,
                        'conditions' => $expr->eq('rel.id', 'type.id')
                    )
                ),
                'conditions' => array(
                    'or' => $expr->orX($expr->eq('type.id', $types[0]->getId()), $expr->eq('type.id', $types[1]->getId())),
                    'and' => $expr->andX($expr->eq('rel.name', $expr->literal($value['conditions']['name'])), $expr->eq('rel.surname', $expr->literal($value['conditions']['surname'])))
                )
            );
            $readData = $em->getRepository($value['bundle'])->findByRelationship($query);

            $this->assertInstanceOf($value['className'], $readData);

            try {
                $this->assertTrue($em->getFilters()
                    ->isEnabled(Common\Gedmo::FilterSoftDeleteable));
                $em->remove($readData);
                $em->flush();

                $em->getFilters()->disable(Common\Gedmo::FilterSoftDeleteable);
                $expr = new Expr();
                $query = array(
                    'bundle' => $value['bundle'],
                    'relationships' => array(
                        'TypeCast' => array(
                            'entity' => TypeCast::getBundle(),
                            'alias' => 'type',
                            'condition_type' => Expr\Join::WITH,
                            'conditions' => $expr->eq('rel.id', 'type.id')
                        )
                    ),
                    'conditions' => array(
                        'or' => $expr->orX($expr->eq('type.id', $types[0]->getId()), $expr->eq('type.id', $types[1]->getId())),
                        'and' => $expr->andX($expr->eq('rel.name', $expr->literal($value['conditions']['name'])), $expr->eq('rel.surname', $expr->literal($value['conditions']['surname'])))
                    )
                );
                $obModificated = $em->getRepository($value['bundle'])->findByRelationship($query);
                $insertedDate = $obModificated->getEndDate();
                $now = new \DateTime();
                $message = 'The data is not removed.';
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }
}