<?php
/**
 * @file
 * Entities tests.
 */
namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Builders\Entity\ContentMedia;
use AppBundle\Entity\Builders\Entity\MasterTable;
use AppBundle\Entity\Enum\Genre as EnumGenre;
use AppBundle\Entity\Enum\TypeCast as EnumTypeCast;
use AppBundle\Entity\Series;
use AppBundle\Tests\Entity\Common\ICRUDEntitiesTest;
use AppBundle\Util\Enum as Common;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CRUDSeriesTest extends WebTestCase implements ICRUDEntitiesTest
{

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::createTestData()
     */
    public function createTestData()
    {
        $data = array(
            array(
                'className' => Series::class,
                'bundle' => Series::getBundle(),
                'title' => 'Doctor Who',
                'synopsis' => "Doctor Who is a British science-fiction television programme produced by the BBC from 1963 to the present day. The programme depicts the adventures of the Doctor, a Time Lord— a space and time-travelling humanoid alien. He explores the universe in his TARDIS, a sentient time-travelling space ship. Its exterior appears as a blue British police box, which was a common sight in Britain in 1963 when the series first aired. Accompanied by companions, the Doctor combats a variety of foes, while working to save civilisations and help people in need.",
                'translation' => array(
                    'es' => array(
                        'synopsis' => "El doctor Who es un programa de televisión de ciencia-ficción británica producida por la BBC desde 1963 hasta nuestros días. El programa muestra las aventuras del Doctor, un Señor- tiempo un espacio y alienígena humanoide viajando en el tiempo. Explora el universo en su TARDIS, una nave espacial viajando en el tiempo sensible. Su exterior aparece como una caja de policía británica azul, que era una vista común en Gran Bretaña en 1963, cuando la serie se estrenó. Acompañado por los compañeros, el Doctor combate una gran variedad de enemigos, mientras se trabaja para salvar civilizaciones y ayudar a las personas necesitadas."
                    )
                ),
                'year' => '2005',
                'country' => 'United Kingdom',
                'genres' => array(
                    0 => array(
                        'code' => EnumGenre::ScienceFiction
                    ),
                    1 => array(
                        'code' => EnumGenre::Adventure
                    ),
                    2 => array(
                        'code' => EnumGenre::Drama
                    )
                ),
                'casts' => array(
                    0 => array(
                        'name' => 'Peter Dougan',
                        'surname' => 'Capaldi',
                        'characters' => array(
                            0 => array(
                                'name' => 'The Doctor'
                            )
                        )
                    ),
                    1 => array(
                        'name' => 'Jenna Louise',
                        'surname' => 'Coleman',
                        'biography' => "Jenna-Louise Coleman (born 27 April 1986), known as Jenna Coleman, is an English actress. She currently plays Clara Oswald in the British television series Doctor Who and has played Jasmine Thomas in the British soap opera Emmerdale. Coleman was born in Blackpool, Lancashire, and began her acting career at an early age as a member of a theatre company called In Yer Space. While auditioning for drama schools in 2005, she was chosen to play Jasmine Thomas in Emmerdale. She received critical acclaim for her performance and was nominated for the Most Popular Newcomer award at the 2006 National Television Awards.",
                        'translation' => array(
                            'es' => array(
                                'biography' => "Jenna-Louise Coleman (nacido el 27 de abril de 1986), conocido como Jenna Coleman, es una actriz Inglés. Ella actualmente juega Clara Oswald en la serie de televisión británica doctor Who y ha jugado Jasmine Thomas en la telenovela británica Emmerdale. Coleman nació en Blackpool, Lancashire, y comenzó su carrera como actriz en una edad temprana como miembro de una compañía de teatro llamada En Yer Espacio. Mientras audiciones para escuelas de arte dramático en 2005, fue elegida para jugar Jasmine Thomas en Emmerdale. Recibió elogios de la crítica por su actuación y fue nominado para el premio más popular Revelación en los Premios de Televisión Nacional de 2006."
                            )
                        ),
                        'birth_date' => '1986-03-27',
                        'types_cast' => array(
                            0 => array(
                                'code' => EnumTypeCast::ActorActress
                            )
                        )
                    )
                ),
                'characters' => array(
                    0 => array(
                        'name' => 'The Doctor'
                    ),
                    1 => array(
                        'name' => 'Clara',
                        'surname' => 'Oswald',
                        'biography' => 'Clara Oswald is the occasional "travelling assistant" to the Eleventh Doctor. The mysterious woman who is first introduced in "Asylum of the Daleks" as "Oswin Oswald". In this episode she is found out to be a human-Dalek with her old memories who hacks the entire Dalek race and wipes their memory of the Doctor. Clara is then reintroduced in "The Snowmen" as merely "Clara", working two jobs, one as a nanny and the other as a barmaid. Clara chases down the Doctor several times and forces him into returning to his former ways of saving the world. She also has a history of saving the Doctor. The Doctor thinks of her as "the only mystery worth solving." Background: Clara Oswald was born in Lancashire, England. She would often lose things, then close her eyes and locate the item. Clara Oswin Oswald is an anagram for "A window across all".',
                        'translations' => array(
                            'es' => array(
                                'biography' => 'Clara Oswald es el ocasional "asistente viajar" al undécimo Doctor. La misteriosa mujer que se introdujo por primera vez en el "Asilo de los Daleks" como "Oswin Oswald". En este episodio ella se enteró de que un ser humano-Dalek con sus viejos recuerdos que hackea toda la raza Dalek y se limpia la memoria del doctor. Clara se reintroduce a continuación, en "The muñecos de nieve" como meramente "Clara", dos trabajos, uno como niñera y el otro como una camarera. Clara persigue a los médico varias veces y le obliga a regresar a sus antiguos caminos de la salvación del mundo. Ella también tiene una historia de salvar al doctor. El doctor piensa en ella como "el único misterio vale la pena resolver." Antecedentes: Clara Oswald nació en Lancashire, Inglaterra. Ella solía perder las cosas, a continuación, cierre los ojos y localizar el artículo. Clara Oswin Oswald es un anagrama de "una ventana a través de todos".'
                            )
                        )
                    )
                ),
                'seasons' => array(
                    0 => array(
                        'code' => MasterTable\SeasonBuilder::CODE_SEASON1
                    )
                ),
                'states_series' => array(
                    0 => array(
                        'code' => MasterTable\StateSeriesBuilder::CODE_IN_BROADCASTING
                    )
                ),
                'chapters' => array(
                    0 => array(
                        'title' => 'Rose',
                        'synopsis' => 'Rose Tyler is just an ordinary shop worker living an ordinary life in 21st century Britain. But that life is turned upside down when a strange man calling himself The Doctor drags her into an alien invasion attempt!',
                        'duration' => '45',
                        'date_of_issue' => '2006-01-19',
                        'weight' => '1',
                        'translations' => array(
                            'es' => array(
                                'synopsis' => 'Rose Tyler es sólo una trabajadora viviendo una vida ordinaria en el siglo 21 Gran Bretaña. Pero que la vida da un vuelco cuando un extraño hombre que se hace llamar el doctor le arrastra a un intento de invasión alienígena!'
                            )
                        ),
                        'casts' => array(
                            0 => array(
                                'name' => 'Keith',
                                'surname' => 'Boak',
                                'biography' => "Keith Boak is a British television director, best known for his work on several popular continuing drama series during the 1990s and 2000s. His career began in 1991 when he directed a short feature 'The Return of Neville Dedd' for the youth programming strand Def II on the BBC Two network. Since then he has worked on drama programmes for all of the major British television networks, including several police dramas such as the BBC's Out of the Blue (1995), City Central (1998) and Mersey Beat (2001). He has also directed medical dramas such as The Royal (ITV, 2003) and Holby City (BBC One, 2004).",
                                'translations' => array(
                                    'es' => array(
                                        'biography' => "Keith Boak es un director de televisión británico, más conocido por su trabajo en varias series de drama Continua popular durante los años 1990 y 2000. Su carrera comenzó en 1991 cuando dirigió un cortometraje 'El regreso de Neville Dedd' de la hebra de programación juvenil Def II en la red BBC Two. Desde entonces ha trabajado en programas de teatro para todas las principales cadenas de televisión británicas, incluyendo varios dramas policiales como fuera de la BBC del Azul (1995), Central City (1998) y Mersey Beat (2001). También ha dirigido dramas médicos como The Royal (ITV, 2003) y Holby City (BBC, 2004)."
                                    )
                                ),
                                'birth_date' => NULL,
                                'types_cast' => array(
                                    0 => array(
                                        'code' => EnumTypeCast::Director
                                    ),
                                    1 => array(
                                        'code' => EnumTypeCast::Producer
                                    )
                                )
                            ),
                            1 => array(
                                'name' => 'Russell T',
                                'surname' => 'Davies',
                                'biography' => "Russell T Davies was born in Swansea, Wales (UK) in 1963. After initially taking a BBC Television director's course in the 1980s, he briefly moved in front of the cameras to present a single episode of the BBC's version of the Australian young children's show 'Play School' in 1987, before deciding that his abilities lay in production rather than presenting. Working for the children's department at BBC Manchester, from 1988 to 1992 he was the producer of summertime activity show 'Why Don't You?' which ironically showcased various things children could be doing rather than sitting at home watching the television. While serving as the producer of 'Why Don't You?' he also made his first forays into writing for television, creating a children's sketch show for early Saturday mornings on BBC One called 'Breakfast Serials' (1990).",
                                'translations' => array(
                                    'es' => array(
                                        'biography' => "Russell T Davies nació en Swansea, Gales (Reino Unido) en 1963. Después de tomar inicialmente un curso de director de televisión de la BBC en la década de 1980, se trasladó brevemente delante de las cámaras para presentar un solo episodio de la versión de la BBC del espectáculo infantil jóvenes australianos 'Play School' en 1987, antes de decidir que sus habilidades yacían en la producción en lugar de presentar. Trabajando para el departamento de niños en la BBC de Manchester, de 1988 a 1992 fue el productor de verano la actividad espectáculo '¿por qué no?' que, irónicamente, exhibió varios niños lo que podrían estar haciendo en lugar de sentarse en casa viendo la televisión. Mientras se desempeñaba como el productor de '¿por qué no?' él también hizo sus primeras incursiones en la escritura para la televisión, creando demostración del bosquejo de los niños para los primeros sábados por la mañana en la BBC llamado 'Seriadas habitaciones' (1990)."
                                    )
                                ),
                                'birth_date' => '1963-03-27',
                                'types_cast' => array(
                                    0 => array(
                                        'code' => EnumTypeCast::Screenwriter
                                    ),
                                    1 => array(
                                        'code' => EnumTypeCast::Producer
                                    )
                                )
                            ),
                            2 => array(
                                'name' => 'Christopher',
                                'surname' => 'Eccleston',
                                'biography' => "Christopher Eccleston (born 16 February 1964) is an English actor. Eccleston played the Ninth Doctor in the British television series Doctor Who and is currently on the American drama series The Leftovers on HBO. He has also appeared on stage and in films such as Let Him Have It, Shallow Grave, Jude, Elizabeth, Gone in 60 Seconds, The Others, 28 Days Later, The Seeker: The Dark Is Rising and Thor: The Dark World. Other British television series he has appeared in include Cracker, Fortitude and The Shadow Line.",
                                'translations' => array(
                                    'es' => array(
                                        'biography' => "Christopher Eccleston (nacido el 16 de febrero 1964) es un actor Inglés. Eccleston jugó el Noveno Doctor en la serie de televisión británica doctor Who y se encuentra actualmente en la serie dramática estadounidense The Leftovers en HBO. También ha aparecido en el escenario y en películas como Let Him Have It, Shallow Grave, Jude, Elizabeth, Gone in 60 Seconds, Los Otros, 28 días después, The Seeker: The Dark Is Rising y Thor: The Dark World. Otra serie de televisión británica ha aparecido en incluir Cracker, Fortaleza y The Shadow Line."
                                    )
                                ),
                                'birth_date' => '1964-02-16',
                                'types_cast' => array(
                                    0 => array(
                                        'code' => EnumTypeCast::ActorActress
                                    )
                                ),
                                'characters' => array(
                                    0 => array(
                                        'name' => 'The Doctor'
                                    )
                                )
                            ),
                            3 => array(
                                'name' => 'Billie',
                                'surname' => 'Piper',
                                'biography' => "Billie Paul Piper (born Leian Paul Piper; 22 September 1982) is an English singer, dancer and actress. She made her debut in Scratchy & Co. (1995–1998), and at the age of 15, she signed a recording contract and released her debut single 'Because We Want To', which debuted at number one in the UK and made her the youngest artist ever to debut at number one. The single was followed by Piper's album Honey to the B (1998), which was certified double platinum by the Recording Industry Association of New Zealand (RIANZ) and platinum by the British Phonographic Industry (BPI). In 2000, she released her second album, Walk of Life. In 2003, she retired from the recording industry and launched an acting career.",
                                'translations' => array(
                                    'es' => array(
                                        'biography' => "Billie Paul Piper (nacido Leian Paul Piper, 22 septiembre de 1982) es una cantante de Inglés, bailarina y actriz. Ella hizo su debut en Scratchy & Co. (1995-1998), ya la edad de 15 años, firmó un contrato de grabación y lanzó su single debut 'porque queremos', que debutó en el número uno en el Reino Unido y la hizo el artista más joven en debutar en el número uno. El single fue seguido por el álbum de Piper miel a la B (1998), que fue certificado doble platino por la Asociación de la Industria Discográfica de Nueva Zelanda (RIANZ) y platino por la Industria Fonográfica Británica (BPI). En 2000, lanzó su segundo álbum, Paseo de la Vida. En 2003, se retiró de la industria discográfica y lanzó una carrera como actriz."
                                    )
                                ),
                                'birth_date' => '1982-09-22',
                                'types_cast' => array(
                                    0 => array(
                                        'code' => EnumTypeCast::ActorActress
                                    )
                                ),
                                'characters' => array(
                                    0 => array(
                                        'name' => 'Rose',
                                        'surname' => 'Tyler',
                                        'biography' => "Rose Marion Tyler was born to parents, Jackie and Pete, on 27th April 1987. After winning the bronze medal for gymnastics, she proved to be a teenage delinquent when she set fire to her gymnasium at her school. She fell in love with soon-to-be mechanic Mickey Smith, whom she dated for years until finally meeting a mysterious stranger who swept her off her feet - the Doctor. After helping him destroy the Nestene Counsciousness after their invasion of London using living plastic, also known as Autons, she joined him in his time traveling 'Police Public Call Box', AKA The TARDIS (Time And Relative Dimension In Space).",
                                        'translations' => array(
                                            'es' => array(
                                                'biography' => "Rose Marion Tyler nació de padres, Jackie y Pete, el 27 de abril de 1987. Después de ganar la medalla de bronce para la gimnasia, que resultó ser un delincuente adolescente cuando prendió fuego a su gimnasio de su escuela. Ella se enamoró de pronto-a-ser mecánico Mickey Smith, quien con fecha durante años hasta que finalmente conocer a un misterioso desconocido que la arrastró a sus pies - el Doctor. Después de ayudar a destruir el Nestene Counsciousness después de su invasión de Londres usando plástico vivir, también conocido como Autons, ella se unió a él en su tiempo viajando 'Policía Pública Call Box', también conocido como El TARDIS (Tiempo Y Dimensión Relativa en el espacio)."
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
                'conditions' => array(
                    'title' => 'Doctor Who',
                    'year' => '2005'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::create()
     * @test
     */
    public function create()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();
        $data = $this->createTestData();
        if (! $data->isEmpty()) {
            foreach ($data->toArray() as $value) {
                $seriesBuilder = new ContentMedia\SeriesBuilder($em, $value['className']);
                $builder = new ContentMedia\ContentMediaSeriesBuilder($seriesBuilder);
                $object = $builder->build($value['title'], $value['synopsis'], $value['year'], $value['country'], $value['genres'], $value['casts'], $value['characters'], NULL, NULL, $value['seasons'], $value['states_series'], $value['chapters']);
                $object = $builder->addTranslation($object, $em, $value['translation']);
                $result = $builder->getBuilder()->persist();
                $this->assertTrue($result);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::readTestData()
     */
    public function readTestData()
    {
        $data = array(
            array(
                'className' => Series::class,
                'bundle' => Series::getBundle(),
                'conditions' => array(
                    'title' => 'Doctor Who',
                    'year' => '2005'
                )
            )
        );

        return new ArrayCollection($data);
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::read()
     * @test
     */
    public function read()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();

        $this->assertTrue((! $data->isEmpty()));

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            foreach ($value['conditions'] as $field => $value) {
                $property = 'get' . ucwords($field);
                $message = 'The data does not match';
                $this->assertSame($value, $readData->$property(), $message);
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::update()
     * @test
     */
    public function update()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            $title = $readData->getTitle() . 'PO';
            $readData->setTitle($title);

            try {
                $em->persist($readData);
                $em->flush();

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

                $insertedDate = $obModificated->getModificationDate();
                $message = 'The date not be modificated ' . $insertedDate->format('Y-m-d');
                $now = new \DateTime();
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }

    /**
     * Non-PHPdoc.
     *
     * @see \AppBundle\Tests\Common\ICRUDEntitiesTest::delete()
     * @test
     */
    public function delete()
    {
        $client = static::createClient();
        $em = $client->getContainer()
            ->get(Common\Service::DoctrineIdentifier)
            ->getManager();

        $data = $this->readTestData();
        $this->assertTrue((! $data->isEmpty()));
        $em->getConnection()->beginTransaction();

        foreach ($data->toArray() as $value) {
            $readData = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);

            $this->assertInstanceOf($value['className'], $readData);

            try {
                $this->assertTrue($em->getFilters()
                    ->isEnabled(Common\Gedmo::FilterSoftDeleteable));
                $em->remove($readData);
                $em->flush();

                $em->getFilters()->disable(Common\Gedmo::FilterSoftDeleteable);

                $obModificated = $em->getRepository($value['bundle'])->findOneBy($value['conditions']);
                $insertedDate = $obModificated->getEndDate();
                $now = new \DateTime();
                $message = 'The data is not removed.';
                $this->assertSame($now->format('Y-m-d'), $insertedDate->format('Y-m-d'), $message);
                $em->getConnection()->rollback();
            }
            catch (Exception $e) {
                $em->getConnection()->rollback();
            }
        }
    }
}